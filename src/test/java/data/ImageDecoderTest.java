package data;

import static java.nio.ByteBuffer.wrap;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import freescape.Palette;

class ImageDecoderTest {

	@ParameterizedTest
	@ValueSource(strings = { //
//		"/mnt/daten/Spiele/Freescape/Castle Master", //
//		"/mnt/daten/Spiele/Freescape/Castle Master 2", //
		"/mnt/daten/Spiele/Freescape/Total Eclipse/SCN1E.DAT" //
	})
	void testDecode(String dataFile) throws IOException {
		File imageFile = new File(dataFile);
		Assumptions.assumeTrue(imageFile.exists());
		ImageDecoder decoder = new ImageDecoder();
		byte[] b = Files.readAllBytes(imageFile.toPath());
		BufferedImage image = decoder.decode(wrap(b), Palette.EGA_PALETTE);
		assertNotNull(image);
	}

}
