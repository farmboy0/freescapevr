package data;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class DataContainerFactoryTest {

	@ParameterizedTest
	@ValueSource(strings = { //
		"/mnt/daten/ScummVM/Freescape/Castle Master (English DOS VirtualWorlds)", //
		"/mnt/daten/ScummVM/Freescape/Castle Master 2 (DOS)", //
		"/mnt/daten/ScummVM/Freescape/Total Eclipse (English DOS)" //
	})
	void testCreating(String dataFile) throws IOException {
		File gameDir = new File(dataFile);
		Assumptions.assumeTrue(gameDir.exists());
		DataContainerFactory fac = new DataContainerFactory();
		DataContainer container = fac.readFrom(gameDir);
		assertNotNull(container);
	}
}
