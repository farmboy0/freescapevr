package engine;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.Optional;

import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.joml.Vector3i;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import data.DataContainer;
import data.DataContainerFactory;
import freescape.Area;
import freescape.AreaObject;
import freescape.script.FCLGameState;
import openxr.Stage;

class PlayerTest {
	private DataContainer container;
	private Stage stage;
	private AreaProviderImpl areaProvider;
	private Player player;

	@BeforeEach
	public void setUp() throws Exception {
		File cmDir = new File("/mnt/daten/ScummVM/Freescape/Castle Master (DOS)");
		Assumptions.assumeTrue(cmDir.exists());
		DataContainerFactory fac = new DataContainerFactory();
		container = fac.readFrom(cmDir);
		stage = new Stage();
		stage.setPlayerStage(new Vector3f(1f), new Quaternionf());
		areaProvider = new AreaProviderImpl();
		player = new Player(stage, areaProvider, new FCLGameStateImpl());
		player.setMoveMode(container.defaultMovement());
	}

	@Test
	void testMoveForward() {
		areaProvider.areaId = 4;
		Area area4 = container.getArea(4);
		AreaObject e2 = area4.getEntrance(2).orElseThrow();

		player.setAreaScale(area4.getScale());
		player.moveTo(new Vector3i(e2.getPos()).setComponent(0, -e2.getPos().x()), e2.getSize());
		for (int i = 0; i < 11; i++) {
			player.move(MovementAction.MOVE_FOWARD);
		}
		assertEquals(128f, stage.getPlayerWorldPosition().z());
	}

	@Test
	void testMoveBackwards() {
		areaProvider.areaId = 4;
		Area area4 = container.getArea(4);
		AreaObject e2 = area4.getEntrance(2).orElseThrow();

		player.setAreaScale(area4.getScale());
		player.moveTo(new Vector3i(e2.getPos()).setComponent(0, -e2.getPos().x()), e2.getSize());
		for (int i = 0; i < 11; i++) {
			player.move(MovementAction.MOVE_BACKWARD);
		}
		assertEquals(3936f, stage.getPlayerWorldPosition().z());
	}

	@Test
	void testStepLeft() {
		areaProvider.areaId = 4;
		Area area4 = container.getArea(4);
		AreaObject e2 = area4.getEntrance(2).orElseThrow();

		player.setAreaScale(area4.getScale());
		player.moveTo(new Vector3i(e2.getPos()).setComponent(0, -e2.getPos().x()), e2.getSize());
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		for (int i = 0; i < 11; i++) {
			player.move(MovementAction.STEP_LEFT);
		}
		assertEquals(-2400f, stage.getPlayerWorldPosition().x());
	}

	@Test
	void testStepRight() {
		areaProvider.areaId = 4;
		Area area4 = container.getArea(4);
		AreaObject e2 = area4.getEntrance(2).orElseThrow();

		player.setAreaScale(area4.getScale());
		player.moveTo(new Vector3i(e2.getPos()).setComponent(0, -e2.getPos().x()), e2.getSize());
		for (int i = 0; i < 11; i++) {
			player.move(MovementAction.STEP_RIGHT);
		}
		assertEquals(-128f, stage.getPlayerWorldPosition().x());
	}

	@Test
	void testArea7() {
		areaProvider.areaId = 7;
		Area area7 = container.getArea(7);
		AreaObject e1 = area7.getEntrance(1).orElseThrow();

		player.setAreaScale(area7.getScale());
		player.moveTo(new Vector3i(e1.getPos()).setComponent(0, -e1.getPos().x()), e1.getSize());
		player.move(MovementAction.STEP_LEFT);
		player.move(MovementAction.STEP_LEFT);
		player.move(MovementAction.STEP_LEFT);
		player.move(MovementAction.STEP_LEFT);
		player.move(MovementAction.STEP_LEFT);
		player.move(MovementAction.STEP_LEFT);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		assertEquals(744f, stage.getPlayerWorldPosition().z());
	}

	@Test
	void testArea7Forward() {
		areaProvider.areaId = 7;
		Area area7 = container.getArea(7);
		AreaObject e1 = area7.getEntrance(1).orElseThrow();

		player.setAreaScale(area7.getScale());
		player.moveTo(new Vector3i(e1.getPos()).setComponent(0, -e1.getPos().x()), e1.getSize());
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		player.move(MovementAction.MOVE_FOWARD);
		assertEquals(1896f, stage.getPlayerWorldPosition().z());
	}

	private static final class FCLGameStateImpl implements FCLGameState {

		@Override
		public void setFalse(int booleanId) {
		}

		@Override
		public void setTrue(int booleanId) {
		}

		@Override
		public void toggleBoolean(int booleanId) {
		}

		@Override
		public void changeStrength(int strength) {
		}

		@Override
		public boolean getBoolean(int booleanId) {
			return false;
		}

		@Override
		public void triggerAnimation(int objectId) {
		}

		@Override
		public Optional<AreaObject> getObject(int objectId) {
			return Optional.empty();
		}

		@Override
		public Optional<AreaObject> getObject(int areaId, int objectId) {
			return Optional.empty();
		}

		@Override
		public int getVariable(int variableId) {
			return 0;
		}

		@Override
		public void increaseScore(int score1, int score2, int score3) {
		}

		@Override
		public void moveToArea(int areaId, int entranceId) {
		}

		@Override
		public void pause(int length) {
		}

		@Override
		public void playSfx(int sfxId) {
		}

		@Override
		public void playSfxNextFrame(int sfxId) {
		}

		@Override
		public void showEffect(int code) {
		}

		@Override
		public void showMessage(int msgId) {
		}

		@Override
		public void decrement(int variableId) {
		}

		@Override
		public void increment(int variableId) {
		}

		@Override
		public void setVariable(int variableId, int value) {
		}
	}

	private class AreaProviderImpl implements AreaProvider {
		public int areaId;

		@Override
		public Area getCurrentArea() {
			return container.getArea(areaId);
		}

		@Override
		public Area getSharedArea() {
			return container.getArea(255);
		}
	}
}
