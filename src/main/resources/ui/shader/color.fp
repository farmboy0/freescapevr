#version 450

in vec4 drawColor;

layout (location = 0) out vec4 outputColor;

void main()
{
	outputColor = drawColor;
}
