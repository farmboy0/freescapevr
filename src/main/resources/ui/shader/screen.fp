#version 450

in vec2 textureCoords;

uniform sampler2D sampler;

layout (location = 0) out vec4 outputColor;

void main()
{
	outputColor = texture(sampler, textureCoords);
}
