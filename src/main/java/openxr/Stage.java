package openxr;

import javax.annotation.Nonnull;

import org.joml.Matrix4f;
import org.joml.Matrix4fc;
import org.joml.Quaternionf;
import org.joml.Quaternionfc;
import org.joml.Vector3f;
import org.joml.Vector3fc;

public class Stage {
	private final PlayerView world = new PlayerView();
	private final PlayerView stage = new PlayerView();

	private final Matrix4f player2StageMatrix = new Matrix4f();
	private final Matrix4f stage2PlayerMatrix = new Matrix4f();
	private final Matrix4f player2WorldMatrix = new Matrix4f();
	private final Matrix4f world2PlayerMatrix = new Matrix4f();
	private final Matrix4f stage2WorldMatrix = new Matrix4f();
	private final Matrix4f world2StageMatrix = new Matrix4f();

	private final Vector3f leftAimPosition = new Vector3f();
	private final Quaternionf leftAimRotation = new Quaternionf();
	private final Matrix4f leftAim2StageMatrix = new Matrix4f();
	private final Vector3f rightAimPosition = new Vector3f();
	private final Quaternionf rightAimRotation = new Quaternionf();
	private final Matrix4f rightAim2StageMatrix = new Matrix4f();
	private final Vector3f leftGripPosition = new Vector3f();
	private final Quaternionf leftGripRotation = new Quaternionf();
	private final Matrix4f leftGrip2StageMatrix = new Matrix4f();
	private final Vector3f rightGripPosition = new Vector3f();
	private final Quaternionf rightGripRotation = new Quaternionf();
	private final Matrix4f rightGrip2StageMatrix = new Matrix4f();

	private float yaw;

	@Nonnull
	public Vector3fc getPlayerStagePosition() {
		return stage.position;
	}

	@Nonnull
	public Quaternionfc getPlayerStageOrientation() {
		return stage.orientation;
	}

	public float getPlayerStageHeight() {
		return stage.height;
	}

	@Nonnull
	public Vector3fc getPlayerWorldPosition() {
		return world.position;
	}

	@Nonnull
	public Quaternionfc getPlayerWorldOrientation() {
		return world.orientation;
	}

	public float getPlayerWorldHeight() {
		return world.height;
	}

	@Nonnull
	public Matrix4fc getPlayerToWorldMatrix() {
		return player2WorldMatrix;
	}

	@Nonnull
	public Matrix4fc getStageToWorldMatrix() {
		return stage2WorldMatrix;
	}

	@Nonnull
	public Matrix4fc getWorldToStageMatrix() {
		return world2StageMatrix;
	}

	@Nonnull
	public Vector3fc getLeftAimPosition() {
		return leftAimPosition;
	}

	@Nonnull
	public Quaternionfc getLeftAimRotation() {
		return leftAimRotation;
	}

	@Nonnull
	public Matrix4fc getLeftAimMatrix() {
		return leftAim2StageMatrix;
	}

	@Nonnull
	public Vector3fc getRightAimPosition() {
		return rightAimPosition;
	}

	@Nonnull
	public Quaternionfc getRightAimRotation() {
		return rightAimRotation;
	}

	@Nonnull
	public Matrix4fc getRightAimMatrix() {
		return rightAim2StageMatrix;
	}

	@Nonnull
	public Vector3fc getLeftGripPosition() {
		return leftGripPosition;
	}

	@Nonnull
	public Quaternionfc getLeftGripRotation() {
		return leftGripRotation;
	}

	@Nonnull
	public Matrix4fc getLeftGripMatrix() {
		return leftGrip2StageMatrix;
	}

	@Nonnull
	public Vector3fc getRightGripPosition() {
		return rightGripPosition;
	}

	@Nonnull
	public Quaternionfc getRightGripRotation() {
		return rightGripRotation;
	}

	@Nonnull
	public Matrix4fc getRightGripMatrix() {
		return rightGrip2StageMatrix;
	}

	public void setLeftAim(@Nonnull Vector3fc position, @Nonnull Quaternionfc orientation) {
		leftAimPosition.set(position);
		leftAimRotation.set(orientation);
		updateHandMatrix(leftAim2StageMatrix, position, orientation);
	}

	public void setRightAim(@Nonnull Vector3fc position, @Nonnull Quaternionfc orientation) {
		rightAimPosition.set(position);
		rightAimRotation.set(orientation);
		updateHandMatrix(rightAim2StageMatrix, position, orientation);
	}

	public void setLeftGrip(@Nonnull Vector3fc position, @Nonnull Quaternionfc orientation) {
		leftGripPosition.set(position);
		leftGripRotation.set(orientation);
		updateHandMatrix(leftGrip2StageMatrix, position, orientation);
	}

	public void setRightGrip(@Nonnull Vector3fc position, @Nonnull Quaternionfc orientation) {
		rightGripPosition.set(position);
		rightGripRotation.set(orientation);
		updateHandMatrix(rightGrip2StageMatrix, position, orientation);
	}

	private static void updateHandMatrix(@Nonnull Matrix4f destination, @Nonnull Vector3fc position,
		@Nonnull Quaternionfc orientation) {
		destination.translationRotate(position, orientation);
	}

	public void setPlayerStage(@Nonnull Vector3fc position, @Nonnull Quaternionfc orientation) {
		stage.position.set(position).setComponent(1, 0);
		stage.orientation.set(orientation);
		stage.height = position.y();
		updateStagePlayer();
	}

	public void setPlayerWorldHeight(int playerWorldHeight) {
		world.height = playerWorldHeight;
		updateStagePlayer();
	}

	public void setPlayerWorldPosition(@Nonnull Vector3fc playerWorldPosition) {
		world.position.set(playerWorldPosition);
		updateWorldPlayer();
	}

	public void rotateLeft() {
		rotate(yaw + 5);
	}

	public void rotateRight() {
		rotate(yaw - 5);
	}

	public void rotate(float newYaw) {
		this.yaw = newYaw;
		world.orientation.fromAxisAngleDeg(0, 1, 0, yaw);
		updateWorldPlayer();
	}

	private void updateWorldPlayer() {
		player2WorldMatrix.translationRotate(world.position, world.orientation).invertAffine(world2PlayerMatrix);
		updateWorldToStage();
	}

	private void updateStagePlayer() {
		final float scale = stage.height / world.height;
		player2StageMatrix.translationRotateScale(stage.position, stage.orientation, scale)
			.invertAffine(stage2PlayerMatrix);
		updateWorldToStage();
	}

	private void updateWorldToStage() {
		player2StageMatrix.mulAffine(world2PlayerMatrix, world2StageMatrix).invert(stage2WorldMatrix);
	}

	private static final class PlayerView {
		protected final Vector3f position = new Vector3f();
		protected final Quaternionf orientation = new Quaternionf();
		protected float height;
	}
}
