package opengl;

import java.io.IOException;

import javax.annotation.Nonnull;

import org.lwjgl.opengl.GL11C;
import org.lwjgl.opengl.GL20C;

import ui.FreescapeVRRenderer;

public class ShaderProgram {
	private static final String SHADER_PATH = "/ui/shader/";

	private final int programId;

	public ShaderProgram(@Nonnull String shaderName) throws IOException {
		this.programId = createShaderProgram(shaderName);
	}

	public void useProgram() {
		GL20C.glUseProgram(programId);
	}

	private static int createShaderProgram(String name) throws IOException {
		int vertexShaderId = createShader(name, ShaderType.VERTEX);
		int fragmentShaderId = createShader(name, ShaderType.FRAGMENT);

		int programId = GL20C.glCreateProgram();

		GL20C.glAttachShader(programId, vertexShaderId);
		GL20C.glAttachShader(programId, fragmentShaderId);
		GL20C.glLinkProgram(programId);
		checkProgram(programId);

		GL20C.glDeleteShader(vertexShaderId);
		GL20C.glDeleteShader(fragmentShaderId);

		return programId;
	}

	private static int createShader(String name, ShaderType type) throws IOException {
		final String shaderSource = new String(
			FreescapeVRRenderer.class.getResourceAsStream(SHADER_PATH + name + "." + type.getExt()).readAllBytes());
		int shaderId = GL20C.glCreateShader(type.getType());
		GL20C.glShaderSource(shaderId, shaderSource);
		GL20C.glCompileShader(shaderId);
		checkShader(shaderId);
		return shaderId;
	}

	private static void checkShader(int shaderId) {
		int result = GL20C.glGetShaderi(shaderId, GL20C.GL_COMPILE_STATUS);
		if (result == GL11C.GL_FALSE) {
			throw new IllegalStateException("Compile shader failed: " + GL20C.glGetShaderInfoLog(shaderId, 4096));
		}
	}

	private static void checkProgram(int programId) {
		int result = GL20C.glGetProgrami(programId, GL20C.GL_LINK_STATUS);
		if (result == GL11C.GL_FALSE) {
			throw new IllegalStateException("Link program failed: " + GL20C.glGetProgramInfoLog(programId, 4096));
		}
	}

	private enum ShaderType {
		VERTEX("vp", GL20C.GL_VERTEX_SHADER), //
		FRAGMENT("fp", GL20C.GL_FRAGMENT_SHADER), //
		;

		private final String ext;
		private final int type;

		private ShaderType(String ext, int type) {
			this.ext = ext;
			this.type = type;
		}

		public String getExt() {
			return ext;
		}

		public int getType() {
			return type;
		}
	}
}
