package math;

import javax.annotation.Nonnull;

import org.joml.RayAabIntersection;
import org.joml.Vector3fc;

public interface AABBc {

	Vector3fc getCenter();

	Vector3fc getMax();

	Vector3fc getMin();

	Rectangle xzRect();

	float distance(AABBc other);

	boolean collides(@Nonnull AABBc other);

	boolean intersects(@Nonnull AABBc other);

	boolean intersects(@Nonnull Vector3fc omin, @Nonnull Vector3fc omax);

	boolean intersects(@Nonnull RayAabIntersection rayAabIntersection);
}
