package math;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.Optional;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Seq;

public record Rectangle(float x1, float x2, float y1, float y2) {

	public float xLength() {
		return x2 - x1;
	}

	public float yLength() {
		return y2 - y1;
	}

	public Optional<Rectangle> overlapWith(@Nonnull Rectangle other) {
		final float ox1 = max(x1, other.x1);
		final float ox2 = min(x2, other.x2);
		final float oy1 = max(y1, other.y1);
		final float oy2 = min(y2, other.y2);
		if (ox1 >= ox2 || oy1 >= oy2) {
			return Optional.empty();
		}
		return Optional.of(new Rectangle(ox1, ox2, oy1, oy2));
	}

	public Seq<Rectangle> nonOverlappingRegions(@Nonnull Rectangle other) {
		return overlapWith(other).map(overlap -> {
			Seq<Rectangle> result = API.Seq();
			if (this.x1 < overlap.x1) {
				result = result.append(new Rectangle(x1, overlap.x1, y1, y2));
			}
			if (this.x2 > overlap.x2) {
				result = result.append(new Rectangle(overlap.x2, x2, y1, y2));
			}
			if (this.y1 < overlap.y1) {
				result = result.append(new Rectangle(x1, x2, y1, overlap.y1));
			}
			if (this.y2 > overlap.y2) {
				result = result.append(new Rectangle(x1, x2, overlap.y2, y2));
			}
			return result;
		}).orElseGet(() -> API.Seq(this));
	}

	public boolean contains(float x, float y) {
		return x1 <= x && x <= x2 && y1 <= y && y <= y2;
	}

	@Override
	public String toString() {
		return String.format("<%5.2f:%5.2f>-<%5.2f:%5.2f>", x1, y1, x2, y2);
	}

}
