package math;

import javax.annotation.Nonnull;

import io.vavr.collection.Seq;

import org.joml.RayAabIntersection;
import org.joml.Vector3f;
import org.joml.Vector3fc;

public class AABB implements AABBc {
	private final Vector3f min = new Vector3f();
	private final Vector3f max = new Vector3f();
	private final Vector3f center = new Vector3f();

	private boolean valid = false;

	public AABB() {
	}

	public AABB(@Nonnull Seq<Vector3fc> vertices) {
		set(vertices);
	}

	public void set(@Nonnull Seq<Vector3fc> vertices) {
		valid = !vertices.isEmpty();
		if (valid) {
			min.set(vertices.get());
			max.set(vertices.get());
			vertices.forEach(min::min);
			vertices.forEach(max::max);
		}
	}

	public void set(AABB bounds) {
		valid = false;
		if (bounds.valid) {
			valid = true;
			min.set(bounds.min);
			max.set(bounds.max);
		}
	}

	@Override
	public Vector3fc getCenter() {
		return center;
	}

	@Override
	public Vector3fc getMax() {
		return max;
	}

	@Override
	public Vector3fc getMin() {
		return min;
	}

	@Override
	public Rectangle xzRect() {
		return new Rectangle(min.x(), max.x(), min.z(), max.z());
	}

	@Override
	public float distance(@Nonnull AABBc other) {
		return center.distance(other.getCenter());
	}

	@Override
	public boolean collides(@Nonnull AABBc other) {
		if (!valid) {
			return false;
		}
		return min.x() < other.getMax().x() && max.x() > other.getMin().x() //
			&& min.y() < other.getMax().y() && max.y() > other.getMin().y() //
			&& min.z() < other.getMax().z() && max.z() > other.getMin().z();
	}

	@Override
	public boolean intersects(@Nonnull AABBc other) {
		if (!valid) {
			return false;
		}
		return min.x() <= other.getMax().x() && max.x() >= other.getMin().x() //
			&& min.y() <= other.getMax().y() && max.y() >= other.getMin().y() //
			&& min.z() <= other.getMax().z() && max.z() >= other.getMin().z();
	}

	@Override
	public boolean intersects(@Nonnull Vector3fc omin, @Nonnull Vector3fc omax) {
		if (!valid) {
			return false;
		}
		return min.x() <= omax.x() && max.x() >= omin.x() //
			&& min.y() <= omax.y() && max.y() >= omin.y() //
			&& min.z() <= omax.z() && max.z() >= omin.z();
	}

	@Override
	public boolean intersects(@Nonnull RayAabIntersection rayAabIntersection) {
		if (!valid) {
			return false;
		}
		return rayAabIntersection.test(min.x, min.y, min.z, max.x, max.y, max.z);
	}

	@Override
	public String toString() {
		return String.format("<%5.2f:%5.2f:%5.2f>-<%5.2f:%5.2f:%5.2f>", min.x(), min.y(), min.z(), max.x(), max.y(),
			max.z());
	}
}
