package main;

import java.io.File;

import io.vavr.control.Try;

import data.DataContainerFactory;
import glfw.GLFWRuntime;
import ui.FreescapeVRUI;

public class FreescapeVR {
	public static void main(String[] args) {
		if (args.length != 1) {
			usageAndExit();
		}

		final File gameDir = new File(args[0]);
		if (!gameDir.exists() || !gameDir.isDirectory() || !gameDir.canRead()) {
			System.err.println("Cannot read directory " + gameDir);
			usageAndExit();
		}

		final DataContainerFactory fac = new DataContainerFactory();
		Try.of(() -> fac.readFrom(gameDir)).andThenTry(container -> {
			GLFWRuntime.register((err, desc) -> System.out.println(desc));
			final FreescapeVRUI ui = new FreescapeVRUI();
			ui.run(container);
		}).andFinally(GLFWRuntime::terminate).onFailure(Throwable::printStackTrace);
	}

	private static void usageAndExit() {
		System.err.println("Run with java -jar <freescapevr jar> <directory of Castle Master 1 oe 2>");
		System.exit(1);
	}
}
