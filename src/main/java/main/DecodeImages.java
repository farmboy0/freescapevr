package main;

import static java.nio.file.StandardOpenOption.READ;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import javax.imageio.ImageIO;

import io.vavr.API;
import io.vavr.collection.Seq;

import data.ImageDecoder;
import freescape.Palette;

public class DecodeImages {
	private static final Seq<String> FILES = API.Seq("scn1e.dat", "cme.dat", "cmle.dat", "cmoe.dat", "cre.dat",
		"crle.dat");
	private static final FilenameFilter FILTER = (dir, name) -> FILES.contains(name.toLowerCase());

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			usageAndExit();
		}

		final File gameDir = new File(args[0]);
		if (!gameDir.exists() || !gameDir.isDirectory() || !gameDir.canRead()) {
			System.err.println("Cannot read directory " + gameDir);
			usageAndExit();
		}

		ImageDecoder decoder = new ImageDecoder();
		for (File fileToDecode : gameDir.listFiles(FILTER)) {
			File dest = new File(gameDir, fileToDecode.getName() + ".png");
			try (FileChannel in = FileChannel.open(fileToDecode.toPath(), READ)) {
				ByteBuffer data = ByteBuffer.allocate((int) in.size());
				in.read(data);
				BufferedImage image = decoder.decode(data, Palette.EGA_PALETTE);
				ImageIO.write(image, "png", dest);
			}
		}
	}

	private static void usageAndExit() {
		System.err.println("Usage: java main.DecodeImages <directory of Castle Master 1 or 2 or Total Eclipse>");
		System.exit(1);
	}
}
