package main;

import static java.nio.file.StandardOpenOption.READ;

import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import javax.imageio.ImageIO;

public class FontsImageCreator {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			usageAndExit();
		}

		final File teDir = new File(args[0]);
		if (!teDir.exists() || !teDir.isDirectory() || !teDir.canRead()) {
			System.err.println("Cannot read directory " + teDir);
			usageAndExit();
		}

		try (FileChannel in = FileChannel.open(new File(teDir, "TOTEE.EXE").toPath(), READ)) {
			ByteBuffer data = ByteBuffer.allocate((int) in.size());
			in.read(data);

			byte[] r = new byte[] { (byte) 255, (byte) 0 };
			byte[] g = new byte[] { (byte) 255, (byte) 0 };
			byte[] b = new byte[] { (byte) 255, (byte) 0 };

			IndexColorModel cm = new IndexColorModel(1, 2, r, g, b);
			BufferedImage img = new BufferedImage(8 * 16, 6 * 6, BufferedImage.TYPE_BYTE_BINARY, cm);

			WritableRaster raster = img.getRaster();
			for (int c = 0; c < 84; c++) {
				for (int row = 0; row < 6; row++) {
					int byteVal = data.get(0xd403 + c * 6 + row);
					for (int i = 0; i < 8; i++) {
						int x = 8 * (c % 16) + i;
						int y = 6 * (c / 16) + row;
						int bit = (1 << (7 - i));
						int value = (byteVal & bit) >> (7 - i);
						raster.setSample(x, y, 0, value);
					}
				}
			}

			File dest = new File(teDir, "fonts.png");
			ImageIO.write(img, "png", dest);
		}
	}

	private static void usageAndExit() {
		System.err.println("Usage: java main.FontsImageCreator <directory of Total Eclipse>");
		System.exit(1);
	}
}
