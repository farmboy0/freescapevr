package main;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import io.vavr.API;
import io.vavr.collection.Seq;

import data.castle.Decryptor;

public class DecryptCastleMaster {
	private static final Seq<String> FILES = API.Seq("cmle", "cmlf", "cmlg", "cmedf", "crle", "credf");
	private static final FilenameFilter FILTER = (dir, name) -> FILES.contains(name.toLowerCase());

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			usageAndExit();
		}

		final File cmDir = new File(args[0]);
		if (!cmDir.exists() || !cmDir.isDirectory() || !cmDir.canRead()) {
			System.err.println("Cannot read directory " + cmDir);
			usageAndExit();
		}

		Decryptor decryptor = new Decryptor();
		for (File fileToDecrypt : cmDir.listFiles(FILTER)) {
			File dest = new File(cmDir, fileToDecrypt.getName() + ".decrypt");
			try ( //
				FileChannel in = FileChannel.open(fileToDecrypt.toPath(), READ);
				FileChannel out = FileChannel.open(dest.toPath(), CREATE, WRITE, TRUNCATE_EXISTING) //
			) {
				ByteBuffer data = ByteBuffer.allocate((int) in.size());
				in.read(data);
				decryptor.decrypt(data);
				out.write(data.rewind());
			}
		}
	}

	private static void usageAndExit() {
		System.err.println("Usage: java main.DecryptCastleMaster <directory of Castle Master 1 or 2>");
		System.exit(1);
	}
}
