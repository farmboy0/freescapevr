package data;

import io.vavr.collection.Seq;

public record AreaHeader(int areasCount, int startingArea, int startingEntrance, Seq<String> areaNames,
	int[] colorMapping) {

}
