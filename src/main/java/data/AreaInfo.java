package data;

import io.vavr.collection.Seq;
import io.vavr.collection.SortedMap;

import freescape.Area;
import freescape.script.FCLScript;

public record AreaInfo(AreaHeader header, SortedMap<Integer, Area> areasMap, Seq<FCLScript> globalScripts) {

}
