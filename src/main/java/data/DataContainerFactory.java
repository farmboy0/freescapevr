package data;

import java.io.File;
import java.io.IOException;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Set;

import data.castle.CastleMaster1DataReader;
import data.castle.CastleMaster2DataReader;
import data.teclipse.TotalEclipseDataReader;

public final class DataContainerFactory {
	private static final Set<AbstractFreescapeDataReader> READERS = API.Set( //
		new CastleMaster1DataReader(), //
		new CastleMaster2DataReader(), //
		new TotalEclipseDataReader() //
	);

	public DataContainer readFrom(@Nonnull File gameDir) throws IOException {
		for (AbstractFreescapeDataReader reader : READERS) {
			if (reader.canRead(gameDir)) {
				return reader.readFrom(gameDir);
			}
		}
		throw new IllegalArgumentException(gameDir + " does not contain game files");
	}
}
