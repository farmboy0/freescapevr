package data;

import java.awt.image.BufferedImage;
import java.util.Optional;

import javax.annotation.Nonnull;

import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.SortedSet;

import freescape.Area;
import freescape.ImageType;
import freescape.Message;
import freescape.MovementMode;
import freescape.script.FCLScript;

public final record DataContainer(@Nonnull AreaInfo info, @Nonnull Seq<Message> messages,
	@Nonnull Map<ImageType, BufferedImage> images, @Nonnull Seq<MovementMode> movementModes,
	@Nonnull MovementMode defaultMovement) {

	@Nonnull
	public Optional<BufferedImage> getImage(@Nonnull ImageType imageType) {
		return images.get(imageType).toJavaOptional();
	}

	public int getStartingArea() {
		return info.header().startingArea();
	}

	public int getStartingEntrance() {
		return info.header().startingEntrance();
	}

	@Nonnull
	public Seq<FCLScript> getGlobalScripts() {
		return info.globalScripts();
	}

	public int getAreasCount() {
		return info.header().areasCount();
	}

	@Nonnull
	public SortedSet<Integer> getAreaIds() {
		return info.areasMap().keySet();
	}

	public Area getArea(int id) {
		return info.areasMap().apply(id);
	}
}