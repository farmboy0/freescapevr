package data;

import java.util.Optional;

import javax.annotation.Nonnull;

import io.vavr.collection.Seq;
import io.vavr.collection.SortedMap;

import freescape.Area;
import freescape.AreaObject;
import freescape.group.GroupHandler;
import freescape.script.FCLScript;

public class DataDumper {
	public void dump(@Nonnull AreaInfo info) {
		dump(info.globalScripts(), info.areasMap());
	}

	protected void dump(@Nonnull Seq<FCLScript> globalScripts, @Nonnull SortedMap<Integer, Area> areasMap) {
		dumpGlobalScripts(globalScripts);
		dumpAreas(areasMap);
	}

	protected void dumpGlobalScripts(Seq<FCLScript> globalScripts) {
		globalScripts.forEachWithIndex((script, index) -> {
			System.out.printf("%15s(%3d):%n", "GLOBAL", index + 1);
			System.out.println(script);
		});
	}

	protected void dumpAreas(SortedMap<Integer, Area> areasMap) {
		areasMap.keySet().toArray().map(areasMap::apply).forEach(this::dumpArea);
	}

	protected void dumpArea(Area area) {
		System.out.printf("%15s(%3d): %s%n", "AREA", area.getId(), area.getName());
		if (!area.getScripts().isEmpty()) {
			area.getScripts().forEach(System.out::println);
		}
		area.getEntranceIds()
			.toArray()
			.map(area::getEntrance)
			.map(Optional::orElseThrow)
			.forEach(obj -> dumpObject(area.getId(), obj));
		area.getObjectIds()
			.toArray()
			.map(area::getObject)
			.map(Optional::orElseThrow)
			.forEach(obj -> dumpObject(area.getId(), obj));
		area.getGroups().forEach(group -> dumpGroup(area.getId(), group));
		System.out.println("------------------------");
	}

	protected void dumpObject(int areaId, AreaObject obj) {
		if (obj.getScript() != null)
			System.out.printf("%15s(%3d,%3d):%n%s%n", obj.getType(), areaId, obj.getId(), obj.getScript());
		else
			System.out.printf("%15s(%3d,%3d):%n", obj.getType(), areaId, obj.getId());
	}

	protected void dumpGroup(int areaId, GroupHandler group) {
		System.out.printf("%15s(%3d,%3d):%n", group.getGroup().getType(), areaId, group.getGroup().getId());
		System.out.printf("AreaObject Id=(%3d,%3d)%n", areaId, group.getControlledObjectId1());
		if (group.getControlledObjectId2() != 0)
			System.out.printf("AreaObject Id=(%3d,%3d)%n", areaId, group.getControlledObjectId2());
		if (group.getControlledObjectId3() != 0)
			System.out.printf("AreaObject Id=(%3d,%3d)%n", areaId, group.getControlledObjectId3());
		group.getInstructions().forEach(System.out::println);
	}
}
