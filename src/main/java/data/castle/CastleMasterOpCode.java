package data.castle;

import io.vavr.API;

import freescape.script.FCLOpCode;

enum CastleMasterOpCode {
	OP_0(0, FCLOpCode.NOP), //
	OP_1(1, FCLOpCode.ADDSCORE), //
	OP_3(3, FCLOpCode.TOGVIS), //
	OP_4(4, FCLOpCode.VIS), //
	OP_5(5, FCLOpCode.INVIS), //
	OP_6(6, FCLOpCode.TOGVIS_A), //
	OP_7(7, FCLOpCode.VIS_A), //
	OP_8(8, FCLOpCode.INVIS_A), //
	OP_9(9, FCLOpCode.INCVAR), //
	OP_10(10, FCLOpCode.DECVAR), //
	OP_11(11, FCLOpCode.IF_VAR), //
	OP_12(12, FCLOpCode.SETBIT), //
	OP_13(13, FCLOpCode.CLRBIT), //
	OP_14(14, FCLOpCode.IF_BIT), //
	OP_15(15, FCLOpCode.SOUND), //
	OP_16(16, FCLOpCode.DESTROY), //
	OP_17(17, FCLOpCode.DESTROY_A), //
	OP_18(18, FCLOpCode.MOVETO), //
	OP_19(19, FCLOpCode.ADDSTRGTH), //
	OP_20(20, FCLOpCode.SETVAR), //
	OP_26(26, FCLOpCode.REDRAW), //
	OP_27(27, FCLOpCode.DELAY), //
	OP_28(28, FCLOpCode.SYNCSND), //
	OP_29(29, FCLOpCode.TOGBIT), //
	OP_30(30, FCLOpCode.IF_VIS), //
	OP_31(31, FCLOpCode.IF_INVIS), //
	OP_32(32, FCLOpCode.IF_VIS_A), //
	OP_33(33, FCLOpCode.IF_INVIS_A), //
	OP_34(34, FCLOpCode.PRINT), //
	OP_35(35, FCLOpCode.SCREEN), //
	OP_37(37, FCLOpCode.STARTANIM), //
	OP_41(41, FCLOpCode.LOOP), //
	OP_42(42, FCLOpCode.AGAIN), //
	OP_44(44, FCLOpCode.ELSE), //
	OP_45(45, FCLOpCode.ENDIF), //
	OP_46(46, FCLOpCode.IF_VAR_GT), //
	OP_47(47, FCLOpCode.IF_VAR_LT), //
	OP_48(48, FCLOpCode.EXECUTE), //
	;

	private final int id;
	private final FCLOpCode opcode;

	private CastleMasterOpCode(int id, FCLOpCode opcode) {
		this.id = id;
		this.opcode = opcode;
	}

	public static FCLOpCode fromId(int id) {
		return API.Seq(values())
			.find(op -> op.getId() == id)
			.map(CastleMasterOpCode::getOpcode)
			.getOrElseThrow(() -> new IllegalArgumentException("Unknown opcode id " + id));
	}

	public int getId() {
		return id;
	}

	public FCLOpCode getOpcode() {
		return opcode;
	}
}
