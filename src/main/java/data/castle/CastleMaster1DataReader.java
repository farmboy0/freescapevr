package data.castle;

public class CastleMaster1DataReader extends AbstractCastleMasterDataReader {
	@Override
	protected String getAreasFilename() {
		return "cmedf";
	}

	@Override
	protected String getBorderFilename() {
		return "cme.dat";
	}

	@Override
	protected String getMessagesFilename() {
		return "cmle";
	}

	@Override
	protected String getOptionsFilename() {
		return "cme.exe";
	}

	@Override
	protected String getTitleFilename() {
		return "cmle.dat";
	}
}
