package data.castle;

import freescape.MovementMode;

enum CastleMovement implements MovementMode {
	CRAWL(32, 16), //
	WALK(48, 24), //
	RUN(48, 36), //
	;

	private final int playerHeight;
	private final int stepWidth;

	private CastleMovement(int playerHeight, int stepWidth) {
		this.playerHeight = playerHeight;
		this.stepWidth = stepWidth;
	}

	@Override
	public int getPlayerHeight() {
		return playerHeight;
	}

	@Override
	public int getStepWidth() {
		return stepWidth;
	}
}
