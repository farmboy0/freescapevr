package data.castle;

import static java.nio.ByteOrder.LITTLE_ENDIAN;
import static java.nio.file.StandardOpenOption.READ;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import javax.annotation.Nonnull;

public class Decryptor {
	private static final int DATA_SEGMENT = 0x0055;
	private static final int DECRYPTION_OFFSET = 0x0071;
	private static final int DECRYPTION_SEGMENT = 0x0073;
	private static final int EXTRA_SEGMENT = 0x0077;
	private static final int VALUE_8653 = 0x01F7;

	public void decrypt(@Nonnull ByteBuffer data) {
		for (int i = 3; i < data.limit(); i++) {
			byte b = (byte) (data.get(i) ^ ((0x18 + i) & 0xFF));
			data.put(i, b);
		}
	}

	public ByteBuffer readExeImageFrom(@Nonnull File exeFile) throws IOException {
		try (FileChannel c = FileChannel.open(exeFile.toPath(), READ)) {
			final ByteBuffer file = ByteBuffer.allocate((int) c.size()).order(LITTLE_ENDIAN);
			c.read(file);

			final byte[] signature = new byte[2];

			file.rewind().get(signature);
			if (!new String(signature).equals("MZ")) {
				throw new IOException("not an exe file");
			}

			final int lastPageSize = file.getShort() & 0xFFFF;
			final int pagesCount = file.getShort() & 0xFFFF;
			file.getShort(); // relocations count
			final int headerSizeInParagraphs = file.getShort() & 0xFFFF;

			final int imageOffset = headerSizeInParagraphs * 16;
			final int imageEnd = lastPageSize == 0 ? pagesCount * 512 : (pagesCount - 1) * 512 + lastPageSize;
			final int imageSize = imageEnd - imageOffset;

			final byte[] imageBytes = new byte[imageSize];
			file.position(imageOffset).get(imageBytes);

			return ByteBuffer.wrap(imageBytes).order(LITTLE_ENDIAN);
		}
	}

	private final short[] ivt = new short[10];

	private boolean _df, _if, _cf;
	private int _ss = 0x26B7, _sp = 0x0179, _bp;
	private final int loadseg = 0x110D;
	private final int _cs = 0x26B7;

	/**
	 * @param image the exe image without the exe header
	 */
	public void decryptCMEEXE(@Nonnull ByteBuffer image) {
		int _ax = 0, _bx = 0, _cx = 0, _dx = 0, _si = 0, _di = 0;
		int _ds, _es = 0x10FD; //psp segment

//		26B7:0210	0E	PUSH	CS
//		26B7:0211	1F	POP	DS
		_ds = _cs;
//		26B7:0212	8C 1E 55 00	MOV	[0x0055], DS
		putShort(image, _ds, DATA_SEGMENT, _ds);
//		26B7:0216	8C 06 77 00	MOV	[0x0077], ES
		putShort(image, _ds, EXTRA_SEGMENT, _es);
//		26B7:021A	1E	PUSH	DS
//		26B7:021B	07	POP	ES
		_es = _ds;
//		26B7:021C	BF 5D 00	MOV	DI, 93
		_di = 93;
//		26B7:021F	33 F6	XOR	SI, SI
		_si ^= _si; // =0
//		26B7:0221	8E DE	MOV	DS, SI
		_ds = _si; // =0
//		26B7:0223	B9 0A 00	MOV	CX, 10
		_cx = 10;
//		26B7:0226	FC	CLD	
		_df = false;

		// backup IVT 0-4, 2 words pro interrupt

//		26B7:0227	F3 A5	REP MOVSW	
		while (_cx > 0) {
			putShort(image, _es, _di, ivt[(_ds << 4) + _si]);
			_si += (_df ? -1 : 1); // already words
			_di += (_df ? -2 : 2);
			_cx--;
		}

		// install and call int 3 handler

//		26B7:0229	FA	CLI	
		_if = false;
//		26B7:022A	C7 06 0C 00 79 01	MOV	WORD PTR [0x000C], 377
		ivt[(_ds << 4) + (0x000C >> 1)] = 0x0179; // ds=0
//		26B7:0230	8C 0E 0E 00	MOV	[0x000E], CS
		ivt[(_ds << 4) + (0x000E >> 1)] = (short) _cs; // ds=0
//		26B7:0234	FB	STI	
		_if = true;
//		26B7:0235	CC	INT	3
		int3_handler(image);

//		26B7:0236	0E	PUSH	CS
//		26B7:0237	1F	POP	DS
		_ds = _cs;
//		26B7:0238	A1 77 00	MOV	AX, [0x0077]
		_ax = getShort(image, _ds, EXTRA_SEGMENT); // psp segment
//		26B7:023B	05 10 00	ADD	AX, 16
		_ax += 16; // load segment
//		26B7:023E	A3 73 00	MOV	[0x0073], AX
		putShort(image, _ds, DECRYPTION_SEGMENT, _ax);
//		26B7:0241	A1 F7 01	MOV	AX, [0x01F7]
		_ax = getShort(image, _ds, VALUE_8653);
//		26B7:0244	81 F8 CD 21	CMP	AX, 8653
//		26B7:0248	75 31	JNZ	:027B
		if (_ax == 8653) {
//		26B7:024A	BB 00 00	MOV	BX, 0
			_bx = 0;
//		26B7:024D	BE 00 00	MOV	SI, 0
			_si = 0;
//		26B7:0250	B9 20 00	MOV	CX, 32
			_cx = 32;

			do {
//		26B7:0253	AD	LODSW	
				_ax = getShort(image, _ds, _si);
				_cx--;
//		26B7:0254	03 D8	ADD	BX, AX
				_bx += _ax;
//		26B7:0256	E2 FB	LOOP	:0253
			} while (_cx > 0);
//		26B7:0258	81 FB E2 EF	CMP	BX, 61410
//		26B7:025C	75 1D	JNZ	:027B
//		26B7:025E	EB 15	JMP	:0275
			if (_bx == 61410) {
//		26B7:0275	B9 6D 40	MOV	CX, 16493
				_cx = 16493;
//		26B7:0278	BA 0D BE	MOV	DX, 48653
				_dx = 48653;
			}
		}
//		26B7:027B	BB AA 15	MOV	BX, 5546
		_bx = 5546;
//		26B7:027E	8C D8	MOV	AX, DS
		_ax = _ds;
//		26B7:0280	29 D8	SUB	AX, BX
		_ax -= _bx;
//		26B7:0282	8E D8	MOV	DS, AX
		_ds = _ax;
//		26B7:0284	BE 00 00	MOV	SI, 0
		_si = 0;
//		26B7:0287	B8 04 00	MOV	AX, 4
		_ax = 4;
//		26B7:028A	CC	INT	3
		int3_handler(image);

		do {
//		26B7:028B	D1 E1	SHL	CX, 1
			_cf = (_cx & 0x8000) > 0;
			_cx = (_cx << 1) & 0xFFFF;
//		26B7:028D	F7 C1 02 00	TEST	CX, 2
//		26B7:0291	74 09	JZ	:029C
//		26B7:0293	F7 C1 00 20	TEST	CX, 8192
//		26B7:0297	74 09	JZ	:02A2
//		26B7:0299	EB 0D	JMP	:02A8
//		26B7:029C	F7 C1 00 20	TEST	CX, 8192
//		26B7:02A0	74 06	JZ	:02A8
			boolean cond1 = (_cx & 0x2) == 0 && (_cx & 0x2000) != 0;
			boolean cond2 = (_cx & 0x2) != 0 && (_cx & 0x2000) == 0;
			if (cond1 || cond2) {
//		26B7:02A2	83 C1 01	ADD	CX, 1
				_cx = add(_cx, 1);
//		26B7:02A5	83 D2 00	ADC	DX, 0
				_dx = adc(_dx, 0);
			}

//		26B7:02A8	01 0C	ADD	[SI], CX
			int tmp = getShort(image, _ds, _si);
			tmp = add(tmp, _cx);
			putShort(image, _ds, _si, tmp);
//		26B7:02AA	11 54 02	ADC	[SI+2], DX
			tmp = getShort(image, _ds, _si + 2);
			tmp = adc(tmp, _dx);
			putShort(image, _ds, _si + 2, tmp);
//		26B7:02AD	03 0C	ADD	CX, [SI]
			_cx = add(_cx, getShort(image, _ds, _si));
//		26B7:02AF	13 54 02	ADC	DX, [SI+2]
			_dx = adc(_dx, getShort(image, _ds, _si + 2));
//		26B7:02B2	83 C6 04	ADD	SI, 4
			_si = add(_si, 4);

//		26B7:02B5	73 0C	JNB	:02C3
			if (_cf) {
//		26B7:02B7	2E 81 06 73 00 00 10	ADD	WORD PTR CS:[0x0073], 4096
				tmp = getShort(image, _cs, DECRYPTION_SEGMENT);
				tmp += 4096; // load segment +4096 = 1 chunk
				putShort(image, _cs, DECRYPTION_SEGMENT, tmp);
//		26B7:02BE	2E C5 3E 71 00	LDS	DI, CS:[0x0071]
				_di = getShort(image, _cs, DECRYPTION_OFFSET);
				_ds = getShort(image, _cs, DECRYPTION_SEGMENT);
			}

//		26B7:02C3	48	DEC	AX
			_ax--;
//		26B7:02C4	75 C5	JNZ	:028B
			if (_ax == 0) {
//		26B7:02C6	B8 04 00	MOV	AX, 4
				_ax = 4;
//		26B7:02C9	4B	DEC	BX
				_bx--;
//		26B7:02CA	75 BF	JNZ	:028B
				if (_bx == 0) {
//		26B7:02CC	CC	INT	3
					int3_handler(image);
//		26B7:02CD	2E C5 3E 53 00	LDS	DI, CS:[0x0053]
					_di = getShort(image, _cs, 0x0053);
					_ds = getShort(image, _cs, 0x0055);
//		26B7:02D2	C7 06 13 03 10 00	MOV	WORD PTR [0x0313], 16
					putShort(image, _ds, 0x0313, 16);
//		26B7:02D8	A1 77 00	MOV	AX, [0x0077]
					_ax = getShort(image, _ds, 0x0077);
//		26B7:02DB	50	PUSH	AX
//		26B7:02DC	05 A5 15	ADD	AX, 5541
//		26B7:02DF	A3 15 03	MOV	[0x0315], AX
//		26B7:02E2	58	POP	AX
					putShort(image, _ds, 0x0315, _ax + 5541);
//		26B7:02E3	05 EB 2A	ADD	AX, 10987
					_ax += 10987;
//		26B7:02E6	90	NOP	
//		26B7:02E7	CC	INT	3
					int3_handler(image);

//		26B7:02E8	8E D0	MOV	SS, AX
					_ss = _ax;
//		26B7:02EA	BC 26 97	MOV	SP, 38694
					_sp = 38694;

//		26B7:02ED	BE 5D 00	MOV	SI, 93
					_si = 93;
//		26B7:02F0	31 FF	XOR	DI, DI
					_di ^= _di; // =0
//		26B7:02F2	8E C7	MOV	ES, DI
					_es = _di; // =0
//		26B7:02F4	B9 0A 00	MOV	CX, 10
					_cx = 10;
//		26B7:02F7	FC	CLD	
					_df = false;

					// restore IVT 0-4, 2 words pro interrupt 

//		26B7:02F8	F3 A5	REP MOVSW
					while (_cx > 0) {
						ivt[(_es << 4) + (_di >> 1)] = (short) getShort(image, _ds, _si);
						_si += (_df ? -2 : 2);
						_di += (_df ? -1 : 1); // already words
						_cx--;
					}

//		26B7:02FA	31 C0	XOR	AX, AX
					_ax = 0;
//		26B7:02FC	31 DB	XOR	BX, BX
					_bx = 0;
//		26B7:02FE	31 C9	XOR	CX, CX
					_cx = 0;
//		26B7:0300	31 D2	XOR	DX, DX
					_dx = 0;
//		26B7:0302	31 F6	XOR	SI, SI
					_si = 0;
//		26B7:0304	31 FF	XOR	DI, DI
					_di = 0;
//		26B7:0306	31 ED	XOR	BP, BP
					_bp = 0;
//		26B7:0308	A1 77 00	MOV	AX, [0x0077]
					_ax = getShort(image, _ds, 0x0077);
//		26B7:030B	8E C0	MOV	ES, AX
					_es = _ax;
//		26B7:030D	8E D8	MOV	DS, AX
					_ds = _ax;
//		26B7:030F	31 C0	XOR	AX, AX
					_ax = 0;
//		26B7:0311	FB	STI	
					_if = true;

//		26B7:0312	EA 10 00 A2 26	JMP	FAR PTR 26A2:0010
					return;
				}
			}
		} while (true);
	}

	private void int3_handler(ByteBuffer image) {
//		26B7:0179	50					PUSH	AX
//		26B7:017A	56					PUSH	SI
//		26B7:017B	1E					PUSH	DS
		int _ax, _si, _ds;
//		26B7:017C	0E					PUSH	CS
//		26B7:017D	1F					POP	DS
		_ds = _cs;
//		26B7:017E	BE BE 01			MOV	SI, 446
		_si = 0x1BE;
//		26B7:0181	B8 36 00			MOV	AX, 54
		_ax = 54;

		// 26B7:01BE - 26B7:01F4, code of int 1 handler, gets modified here to working instructions

		do {
//		26B7:0184	F6 1C				NEG	BYTE PTR [SI]
			int tmp = getByte(image, _ds, _si);
			tmp *= -1;
			putByte(image, _ds, _si, tmp);
//		26B7:0186	46					INC	SI
			_si++;
//		26B7:0187	48					DEC	AX
			_ax--;
//		26B7:0188	75 FA				JNZ	:0184
		} while (_ax != 0);

		// install int 1 handler with 26B7:01BE

//		26B7:018A	33 C0				XOR	AX, AX
		_ax = 0;
//		26B7:018C	8E D8				MOV	DS, AX
		_ds = _ax; // 0
//		26B7:018E	C7 06 04 00 BE 01	MOV	WORD PTR [0x0004], 446
		ivt[_ds << 4 + (0x0004 >> 1)] = 0x01BE;
//		26B7:0194	8C 0E 06 00			MOV	[0x0006], CS
		ivt[_ds << 4 + (0x0006 >> 1)] = _cs;

//		26B7:0198	8B F4				MOV	SI, SP
		_si = _sp;
//		26B7:019A	36 80 74 0B 03		XOR	BYTE PTR SS:[SI+11], 3
		int tmp = getByte(image, _ss, _si + 11); // _ss == _cs
		tmp ^= 3;
		putByte(image, _ss, _si + 11, tmp); // _ss == _cs
//		26B7:019F	36 F6 44 0B 01		TEST	BYTE PTR SS:[SI+11], 1
//		26B7:01A4	75 32				JNZ	:01D8
		if ((tmp & 1) == 0) {
//		26B7:01A6	36 C5 36 57 00		LDS	SI, SS:[0x0057]
			_si = getShort(image, _ss, 0x0057); // _ss == _cs
			_ds = getShort(image, _ss, 0x0059); // _ss == _cs
//		26B7:01AB	8B 44 FE			MOV	AX, [SI-2]
			_ax = getShort(image, _ds, _si - 2);
//		26B7:01AE	F7 D0				NOT	AX
			_ax = ~_ax;
//		26B7:01B0	D1 C0				ROL	AX, 1
			_ax = _ax << 1 | _ax >> 15;
//		26B7:01B2	33 04				XOR	AX, [SI]
			_ax ^= getShort(image, _ds, _si);
//		26B7:01B4	3C CC				CMP	AL, 204
//		26B7:01B6	74 02				JZ	:01BA
			if ((_ax & 0xFF) != 0xCC) {
//		26B7:01B8	87 04				XCHG	AX, [SI]
				tmp = _ax;
				_ax = getShort(image, _ds, _si);
				putShort(image, _ds, _si, tmp);
			}
//		26B7:01BA	1F					POP	DS
//		26B7:01BB	5E					POP	SI
//		26B7:01BC	58					POP	AX
//		26B7:01BD	CF					IRET
		} else {
//		26B7:01D8	36 C5 74 06			LDS	SI, SS:[SI+6]
			_si = getShort(image, _ss, _si + 6);
			_ds = getShort(image, _ss, _si + 8);
//		26B7:01DC	8B 44 FE			MOV	AX, [SI-2]
			_ax = getShort(image, _ds, _si - 2);
//		26B7:01DF	F7 D0				NOT	AX
			_ax = ~_ax;
//		26B7:01E1	D1 C0				ROL	AX, 1
			_ax = _ax << 1 | _ax >> 15;
//		26B7:01E3	31 04				XOR	[SI], AX
			tmp = getShort(image, _ds, _si);
			tmp ^= _ax;
			putShort(image, _ds, _si, tmp);
///		26B7:01E5	36 89 36 57 00		MOV	SS:[0x0057], SI
			putShort(image, _ss, 0x0057, _si);
//		26B7:01EA	8C D8				MOV	AX, DS
			_ax = _ds;
//		26B7:01EC	36 A3 59 00			MOV	SS:[0x0059], AX
			putShort(image, _ss, 0x0059, _ax);
//		26B7:01F0	1F					POP	DS
//		26B7:01F1	5E					POP	SI
//		26B7:01F2	58					POP	AX
//		26B7:01F3	CF					IRET
		}
	}

	private int getByte(ByteBuffer image, int segment, int offset) {
		return bufpos(image, segment, offset).get() & 0xFF;
	}

	private int getShort(ByteBuffer image, int segment, int offset) {
		return bufpos(image, segment, offset).getShort() & 0xFFFF;
	}

	private void putByte(ByteBuffer image, int segment, int offset, int val) {
		bufpos(image, segment, offset).put((byte) val);
	}

	private void putShort(ByteBuffer image, int segment, int offset, int val) {
		bufpos(image, segment, offset).putShort((short) val);
	}

	private ByteBuffer bufpos(ByteBuffer image, int segment, int offset) {
		int pos = ((segment - loadseg) << 4) + offset;
		return image.position(pos);
	}

	private int add(int v1, int v2) {
		int result = v1 + v2;
		_cf = result > 0xFFFF;
		return result & 0xFFFF;
	}

	private int adc(int v1, int v2) {
		int result = v1 + v2 + (_cf ? 1 : 0);
		_cf = result > 0xFFFF;
		return result & 0xFFFF;
	}
}
