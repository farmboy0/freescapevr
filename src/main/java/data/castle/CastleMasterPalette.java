package data.castle;

import java.awt.Color;

import javax.annotation.Nonnull;

import freescape.Palette;

class CastleMasterPalette extends Palette {

	private StippleColor[] stipples;

	CastleMasterPalette(@Nonnull int[] colorMap, @Nonnull StippleColor[] stipples) {
		super(colorMap);
		this.stipples = stipples;
	}

	@Override
	public Color getColor(int idx) {
		if (idx == 0) {
			return null;
		}
		if (stipples[idx] != null) {
			Color c1 = getColor1(idx);
			Color c2 = getColor2(idx);
			if (c1 == null || c2 == null)
				return null;
			return new Color((c1.getRed() + c2.getRed()) / 2, (c1.getGreen() + c2.getGreen()) / 2,
				(c1.getBlue() + c2.getBlue()) / 2);
		}
		return super.getColor(idx);
	}

	@Override
	public Color getColor1(int idx) {
		if (idx == 0) {
			return null;
		}
		if (stipples[idx] != null) {
			return super.getColor(stipples[idx].color1());
		}
		return super.getColor1(idx);
	}

	@Override
	public Color getColor2(int idx) {
		if (idx == 0) {
			return null;
		}
		if (stipples[idx] != null) {
			return super.getColor(stipples[idx].color2());
		}
		return super.getColor2(idx);
	}

	public int getColorValue1(int idx) {
		if (stipples[idx] != null) {
			return stipples[idx].color1();
		}
		return idx;
	}

	public int getColorValue2(int idx) {
		if (stipples[idx] != null) {
			return stipples[idx].color2();
		}
		return idx;
	}

	@Override
	public boolean isStipple(int idx) {
		if (stipples[idx] != null) {
			return true;
		}
		return super.isStipple(idx);
	}

	record StippleColor(int color1, int color2) {
		StippleColor(int color) {
			this(color & 0xF, (color >> 4) & 0xF);
		}
	}
}
