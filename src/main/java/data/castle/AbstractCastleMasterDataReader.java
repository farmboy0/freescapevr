package data.castle;

import static data.castle.CastleMovement.WALK;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;

import data.AbstractFreescapeDataReader;
import data.AreaHeader;
import data.castle.CastleMasterPalette.StippleColor;
import freescape.Message;
import freescape.MovementMode;
import freescape.Palette;
import freescape.script.FCLOpCode;

abstract class AbstractCastleMasterDataReader extends AbstractFreescapeDataReader {
	private static final Decryptor DECRYPTOR = new Decryptor();

	@Override
	protected ByteBuffer readDataFile(File gameDir, String filename) throws IOException {
		final ByteBuffer data = super.readDataFile(gameDir, filename);
		if (isEncrypted(filename)) {
			DECRYPTOR.decrypt(data);
		}
		return data;
	}

	protected boolean isEncrypted(String filename) {
		return !filename.toLowerCase().endsWith(".dat");
	}

	@Override
	protected Seq<Message> readMessages(File gameDir) throws IOException {
		final ByteBuffer data = readDataFile(gameDir, getMessagesFilename());
		skip(data.rewind(), 16);

		return Array.range(0, 41)
			.map(i -> readMessage(data, 15)) // gameplay messages
			.appendAll(Array.range(0, 66).map(i -> readMessage(data, 15))) // area names
			.appendAll(Array.range(0, 25).map(i -> readMessage(data, 15))) // gameplay messages
			.appendAll(Array.range(0, 2).map(i -> readMessage(data, 12))) // destroyed messages
			.appendAll(Array.range(0, 6).map(i -> readMessage(data, 10))) // strength messages
			.appendAll(Array.range(0, 26).map(i -> readMessage(data, 17))); // gameplay messages
	}

	private Message readMessage(ByteBuffer data, int charCount) {
		final boolean needsOffset = readByte(data) != 0;
		byte[] text = new byte[charCount];
		data.get(text);
		return new Message(needsOffset, new String(text));
	}

	@Override
	protected Seq<String> readAreaNames(Seq<Message> messages) {
		return messages.drop(41).take(66).map(Message::text);
	}

	@Override
	protected String readAreaName(AreaHeader header, ByteBuffer data) {
		skip(data, 10);

		int nameIdx = readByte(data);

		if (nameIdx > 65)
			return "";
		return header.areaNames().get(nameIdx);
	}

	@Override
	protected Palette readAreaPalette(@Nonnull AreaHeader header, @Nonnull ByteBuffer data) {
		skip(data, 6);

		int bgNormal = readByte(data);
		int bgWhenHit = readByte(data);
		int paperColor = readByte(data);
		int inkColor = readByte(data);

		skip(data, 1);

		int extraColor1 = readByte(data);
		int extraColor2 = readByte(data);
		int extraColor3 = readByte(data);
		int extraColor4 = readByte(data);

		StippleColor[] stipples = new StippleColor[16];
		if (extraColor1 != 0)
			stipples[bgWhenHit] = new StippleColor(extraColor1);
		if (extraColor2 != 0)
			stipples[bgNormal] = new StippleColor(extraColor2);
		if (extraColor3 != 0)
			stipples[paperColor] = new StippleColor(extraColor3);
		if (extraColor4 != 0)
			stipples[inkColor] = new StippleColor(extraColor4);
		return new CastleMasterPalette(header.colorMapping(), stipples);
	}

	@Override
	protected FCLOpCode opcodeFrom(int opCodeAndTrigger) {
		return CastleMasterOpCode.fromId(opCodeAndTrigger & 0x3F);
	}

	@Override
	protected int getAreaSize() {
		return 15;
	}

	@Override
	protected int getAreasOffset() {
		return 0;
	}

	@Override
	protected int getBorderOffset() {
		return 0;
	}

	@Override
	protected int getMessagesOffset() {
		return 0;
	}

	@Override
	protected int getOptionsOffset() {
		return 0;
	}

	@Override
	protected int getTitleOffset() {
		return 0;
	}

	@Override
	protected int getSensorOrdCount() {
		return 0;
	}

	@Override
	protected Seq<MovementMode> getMovementModes() {
		return API.Seq(CastleMovement.values());
	}

	@Override
	@Nonnull
	public MovementMode getDefaultMovement() {
		return WALK;
	}
}
