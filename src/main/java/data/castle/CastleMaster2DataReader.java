package data.castle;

public class CastleMaster2DataReader extends AbstractCastleMasterDataReader {
	@Override
	protected String getAreasFilename() {
		return "credf";
	}

	@Override
	protected String getBorderFilename() {
		return "cre.dat";
	}

	@Override
	protected String getMessagesFilename() {
		return "crle";
	}

	@Override
	protected String getOptionsFilename() {
		return "cre.exe";
	}

	@Override
	protected String getTitleFilename() {
		return "crle.dat";
	}
}
