package data;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.IndexColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.annotation.Nonnull;

import io.vavr.collection.Seq;

public class ImageDecoder {
	public BufferedImage decode(@Nonnull ByteBuffer data, @Nonnull Seq<Color> colors) {
		final int length = data.rewind().order(ByteOrder.BIG_ENDIAN).getShort();
		final int start = data.position();

		WritableRaster raster = Raster.createBandedRaster(DataBuffer.TYPE_BYTE, 320, 200, 1, null);
		for (int row = 0; row < 200; row++) {
			readRow(data, raster, row);
		}
		final int end = data.position();

		assert (end - start) == length;

		final IndexColorModel cm = createColorModel(colors);
		return new BufferedImage(cm, raster, true, null);
	}

	private static void readRow(ByteBuffer data, WritableRaster raster, int row) {
		int bit = 1;
		for (int iter = 0; iter < 4; iter++) {
			readIteration(data, raster, row, bit);
			bit <<= 1;
		}
	}

	private static void readIteration(ByteBuffer data, WritableRaster raster, int row, int bit) {
		int pixels = 0;
		while (pixels < 320) {
			pixels += execute(data, raster, row, pixels, bit);
		}
	}

	private static int execute(ByteBuffer data, WritableRaster raster, int row, int column, int bit) {
		final int code = data.get() & 0xFF;
		if (code >= 0xD0) {
			int count;
			if (code >= 0xF0) {
				count = 17 - (code & 0x0F);
			} else if (code >= 0xE0) {
				count = 33 - (code & 0x0F);
			} else {
				count = 34 + (0xDF - code);
			}
			return executeSingle(data, raster, row, column, bit, count);
		} else if ((code & 0x80) == 0) {
			final int count = code + 1;
			return executeMulti(data, raster, row, column, bit, count);
		}
		throw new IllegalStateException("Unknown code: " + code);
	}

	private static int executeSingle(ByteBuffer data, WritableRaster raster, int row, int column, int bit, int count) {
		final int pixel = data.get() & 0xFF;
		for (int i = 0; i < count; i++) {
			changePixels(raster, row, column + 8 * i, pixel, bit);
		}
		return 8 * count;
	}

	private static int executeMulti(ByteBuffer data, WritableRaster raster, int row, int column, int bit, int count) {
		for (int i = 0; i < count; i++) {
			final int pixel = data.get() & 0xFF;
			changePixels(raster, row, column + 8 * i, pixel, bit);
		}
		return 8 * count;
	}

	private static void changePixels(WritableRaster raster, int row, int startcolumn, int pixels, int bit) {
		int mask = 0x80;
		for (int i = 0; i < 8; i++) {
			if ((pixels & mask) > 0) {
				int sample = raster.getSample(startcolumn + i, row, 0) | bit;
				raster.setSample(startcolumn + i, row, 0, sample);
			}
			mask >>= 1;
		}
	}

	private static IndexColorModel createColorModel(@Nonnull Seq<Color> colors) {
		byte[] r = new byte[16];
		byte[] g = new byte[16];
		byte[] b = new byte[16];

		colors.forEachWithIndex((c, i) -> {
			r[i] = (byte) c.getRed();
			g[i] = (byte) c.getGreen();
			b[i] = (byte) c.getBlue();
		});
		return new IndexColorModel(4, 16, r, g, b);
	}
}
