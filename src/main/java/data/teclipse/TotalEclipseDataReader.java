package data.teclipse;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import io.vavr.API;
import io.vavr.collection.Seq;

import data.AbstractFreescapeDataReader;
import data.AreaHeader;
import freescape.Message;
import freescape.MovementMode;
import freescape.script.FCLOpCode;

public class TotalEclipseDataReader extends AbstractFreescapeDataReader {
	@Override
	protected Seq<Message> readMessages(File gameDir) throws IOException {
		return API.Seq();
	}

	@Override
	protected Seq<String> readAreaNames(Seq<Message> messages) {
		return API.Seq();
	}

	@Override
	protected String readAreaName(AreaHeader header, ByteBuffer data) {
		return "";
	}

	@Override
	protected String getAreasFilename() {
		return "totee.exe";
	}

	@Override
	protected String getBorderFilename() {
		return "totee.exe";
	}

	@Override
	protected String getMessagesFilename() {
		return "totee.exe";
	}

	@Override
	protected String getOptionsFilename() {
		return "totee.exe";
	}

	@Override
	protected String getTitleFilename() {
		return "scn1e.dat";
	}

	@Override
	protected int getAreasOffset() {
		return 0x3CE0;
	}

	@Override
	protected int getBorderOffset() {
		return 0x210;
	}

	@Override
	protected int getMessagesOffset() {
		return 0x710F;
	}

	@Override
	protected int getOptionsOffset() {
		return 0;
	}

	@Override
	protected int getTitleOffset() {
		return 0;
	}

	@Override
	protected int getSensorOrdCount() {
		return 5;
	}

	@Override
	protected MovementMode getDefaultMovement() {
		return TotalEclipseMovement.WALK;
	}

	@Override
	protected Seq<MovementMode> getMovementModes() {
		return API.Seq(TotalEclipseMovement.values());
	}

	@Override
	protected int getAreaSize() {
		return 15;
	}

	@Override
	protected FCLOpCode opcodeFrom(int opCodeAndTrigger) {
		return TotalEclipseOpCode.fromId(opCodeAndTrigger & 0x3F);
	}
}
