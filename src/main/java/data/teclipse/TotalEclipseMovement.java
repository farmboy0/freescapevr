package data.teclipse;

import freescape.MovementMode;

enum TotalEclipseMovement implements MovementMode {
	CRAWL(32, 16), //
	WALK(48, 24), //
	RUN(48, 36), //
	;

	private final int playerHeight;
	private final int stepWidth;

	private TotalEclipseMovement(int playerHeight, int stepWidth) {
		this.playerHeight = playerHeight;
		this.stepWidth = stepWidth;
	}

	@Override
	public int getPlayerHeight() {
		return playerHeight;
	}

	@Override
	public int getStepWidth() {
		return stepWidth;
	}
}
