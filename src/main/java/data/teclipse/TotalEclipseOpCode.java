package data.teclipse;

import io.vavr.API;

import freescape.script.FCLOpCode;

enum TotalEclipseOpCode {
	OP_0(0, FCLOpCode.NOP), //
	OP_1(1, FCLOpCode.ADDSCORE), // 9FEF
	OP_2(2, FCLOpCode.ADDSTRGTH), // A02D
	OP_3(3, FCLOpCode.TOGVIS), // A08E A0F7
	OP_4(4, FCLOpCode.VIS), // A093 A104
	OP_5(5, FCLOpCode.INVIS), // A098
	OP_6(6, FCLOpCode.TOGVIS_A), // A18B A1E6
	OP_7(7, FCLOpCode.VIS_A), // A190 A1F3
	OP_8(8, FCLOpCode.INVIS_A), // A195
	OP_9(9, FCLOpCode.INCVAR), // A205 A224
	OP_10(10, FCLOpCode.DECVAR), // A20A A22E
	OP_11(11, FCLOpCode.IF_VAR), // A20F
	OP_12(12, FCLOpCode.SETBIT), // A255 A2A7
	OP_13(13, FCLOpCode.CLRBIT), // A25A A2B3
	OP_14(14, FCLOpCode.IF_BIT), // A25F
	OP_15(15, FCLOpCode.SOUND), // A2CF
	OP_16(16, FCLOpCode.DESTROY), // A09D A0EC
	OP_17(17, FCLOpCode.DESTROY_A), // A19A A1DB
	OP_18(18, FCLOpCode.MOVETO), // A2F3
	OP_19(19, FCLOpCode.ADDENERGY), // A05C
	OP_20(20, FCLOpCode.SETVAR), // A214 A23E
	OP_25(25, FCLOpCode.SPFX), // A35B
	OP_26(26, FCLOpCode.REDRAW), // A39C
	OP_27(27, FCLOpCode.DELAY), // A3AD
	OP_28(28, FCLOpCode.SYNCSND), // A2D4
	OP_29(29, FCLOpCode.TOGBIT), // A264 A299
	OP_30(30, FCLOpCode.IF_VIS), // A0A2 A0BF
	OP_31(31, FCLOpCode.IF_INVIS), // A0A7 A0CD
	OP_32(32, FCLOpCode.IF_VIS_A), // A1BA
	OP_33(33, FCLOpCode.IF_INVIS_A), // A1C8
	OP_34(34, FCLOpCode.PRINT), // A31D
	OP_35(35, FCLOpCode.SCREEN), // A34D
	;

	private final int id;
	private final FCLOpCode opcode;

	private TotalEclipseOpCode(int id, FCLOpCode opcode) {
		this.id = id;
		this.opcode = opcode;
	}

	public static FCLOpCode fromId(int id) {
		return API.Seq(values())
			.find(op -> op.getId() == id)
			.map(TotalEclipseOpCode::getOpcode)
			.getOrElseThrow(() -> new IllegalArgumentException("Unknown opcode id " + id));
	}

	public int getId() {
		return id;
	}

	public FCLOpCode getOpcode() {
		return opcode;
	}
}
