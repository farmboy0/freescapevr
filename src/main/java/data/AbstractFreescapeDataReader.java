package data;

import static freescape.AreaObjectType.ENTRANCE;
import static freescape.AreaObjectType.GROUP;
import static freescape.AreaObjectType.fromId;
import static java.nio.file.StandardOpenOption.READ;
import static java.util.function.Function.identity;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Array;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.SortedMap;

import org.joml.Vector3i;
import org.joml.Vector3ic;

import freescape.Area;
import freescape.AreaObject;
import freescape.AreaObjectType;
import freescape.ImageType;
import freescape.Message;
import freescape.MovementMode;
import freescape.Palette;
import freescape.group.GroupHandler;
import freescape.group.GroupInstruction;
import freescape.group.GroupInstructionPosition;
import freescape.group.GroupInstructionRepeat;
import freescape.group.GroupInstructionScript;
import freescape.script.FCLInstruction;
import freescape.script.FCLOpCode;
import freescape.script.FCLScript;
import freescape.script.FCLTrigger;

public abstract class AbstractFreescapeDataReader {
	private static final DataDumper DUMPER = new DataDumper();

	public boolean canRead(@Nonnull File gameDir) {
		final Seq<String> filenames = API.Seq(getAreasFilename(), getBorderFilename(), getMessagesFilename(),
			getOptionsFilename(), getTitleFilename());
		return API.Seq(gameDir.listFiles((dir, name) -> {
			return filenames.contains(name.toLowerCase());
		})).map(File::getName).map(String::toLowerCase).containsAll(filenames);
	}

	public DataContainer readFrom(@Nonnull File gameDir) throws IOException {
		final Map<ImageType, BufferedImage> images = readImages(gameDir);
		final Seq<Message> messages = readMessages(gameDir);
		final Seq<String> areaNames = readAreaNames(messages);
		final AreaInfo info = readAreas(gameDir, areaNames);

		DUMPER.dump(info);

		return new DataContainer(info, messages, images, getMovementModes(), getDefaultMovement());
	}

	protected Map<ImageType, BufferedImage> readImages(File gameDir) throws IOException {
		final ImageDecoder decoder = new ImageDecoder();
		final ByteBuffer data = readDataFile(gameDir, getTitleFilename()).position(getTitleOffset()).slice();
		final BufferedImage title = decoder.decode(data, Palette.EGA_PALETTE);
		return API.Map(ImageType.TITLE, title);
	}

	protected abstract Seq<Message> readMessages(File gameDir) throws IOException;

	protected AreaInfo readAreas(File gameDir, Seq<String> areaNames) throws IOException {
		final ByteBuffer data = readDataFile(gameDir, getAreasFilename()).position(getAreasOffset());

		final AreaHeader header = readHeader(areaNames, data);

		Seq<FCLScript> globalScripts = readGlobalScripts(data.position(getAreasOffset() + 0x48));

		data.position(getAreasOffset() + 0xC8);
		final SortedMap<Integer, Area> areasMap = Array.range(0, header.areasCount())
			.map(i -> readShort(data))
			.map(offset -> readArea(header, data.position(getAreasOffset() + offset)))
			.toSortedMap(Area::getId, identity());

		return new AreaInfo(header, areasMap, globalScripts);
	}

	protected ByteBuffer readDataFile(File gameDir, String filename) throws IOException {
		final File dataFile = gameDir.listFiles((dir, name) -> name.equalsIgnoreCase(filename))[0];

		try (FileChannel in = FileChannel.open(dataFile.toPath(), READ)) {
			final ByteBuffer data = ByteBuffer.allocate((int) in.size());
			in.read(data);
			return data.order(ByteOrder.LITTLE_ENDIAN).rewind();
		}
	}

	protected AreaHeader readHeader(Seq<String> areaNames, ByteBuffer data) {
		int areaCount = readByte(data);

		int dbSize = readShort(data);

		int startingArea = readByte(data);
		int startingEntrance = readByte(data);

		int checksum = readShort(data);
		int unknown6 = readByte(data);
		int unknown7 = readByte(data);
		int unknown8 = readByte(data);

		int[] colorMapping = readColorMapping(data);

		return new AreaHeader(areaCount, startingArea, startingEntrance, areaNames, colorMapping);
	}

	protected int[] readColorMapping(ByteBuffer data) {
		int[] colorMapping = new int[16];
		colorMapping[0] = -1;
		for (int i = 1; i <= 15; i++) {
			int blue = readByte(data);
			int green = readByte(data);
			int red = readByte(data);
			int intensity = readByte(data);
			int cindex = (intensity & 0x08) | (red & 0x04) | (green & 0x02) | (blue & 0x01);
			colorMapping[i] = cindex;
		}
		return colorMapping;
	}

	protected Seq<FCLScript> readGlobalScripts(ByteBuffer data) {
		int ptr = readShort(data);
		data.position(getAreasOffset() + ptr);
		int scriptCount = readByte(data);
		return Array.range(0, scriptCount).map(i -> readScript(data));
	}

	protected Area readArea(AreaHeader header, ByteBuffer data) {
		final int areaStart = data.position();

		final int areaColors = readByte(data);
		final int objectCount = readByte(data);
		final int id = readByte(data);
		final int scriptsOffset = readShort(data);
		final int scale = readByte(data);

		final int bgNormal = readByte(data);
		final int bgWhenHit = readByte(data);
		final int paperColor = readByte(data);
		final int inkColor = readByte(data);

		final Palette palette = readAreaPalette(header, data.position(areaStart));
		final String areaName = readAreaName(header, data.position(areaStart));

		data.position(areaStart + getAreaSize());

		final Seq<AreaObject> objects = readObjects(id, objectCount, data);
		final SortedMap<Integer, AreaObject> entranceMap = entrancesFrom(objects);
		final SortedMap<Integer, AreaObject> objectsMap = geometryFrom(objects);
		final Seq<GroupHandler> groups = groupsFrom(id, objects);

		final int scriptCount = readByte(data);
		final Seq<FCLScript> scripts = Array.range(0, scriptCount).map(i -> readScript(data));

		final int skyColor = areaColors & 0x0F;
		final int groundColor = (areaColors & 0xF0) >> 4;

		return new Area(id, areaName, scale, entranceMap, objectsMap, scripts, groups, palette, skyColor, groundColor,
			bgNormal, bgWhenHit, paperColor, inkColor);
	}

	protected Palette readAreaPalette(AreaHeader header, ByteBuffer data) {
		return new Palette(header.colorMapping());
	}

	protected int getAreaSize() {
		return 10;
	}

	protected Seq<AreaObject> readObjects(int areaId, int objectCount, ByteBuffer data) {
		return Array.range(0, objectCount).map(i -> readObject(areaId, data));
	}

	protected SortedMap<Integer, AreaObject> entrancesFrom(Seq<AreaObject> objects) {
		return objects.filter(ao -> ENTRANCE.equals(ao.getType())).toSortedMap(AreaObject::getId, identity());
	}

	protected SortedMap<Integer, AreaObject> geometryFrom(Seq<AreaObject> objects) {
		return objects.filter(ao -> !ENTRANCE.equals(ao.getType()) && !GROUP.equals(ao.getType()))
			.toSortedMap(AreaObject::getId, identity());
	}

	protected Seq<GroupHandler> groupsFrom(int areaId, Seq<AreaObject> objects) {
		return objects.filter(obj -> GROUP.equals(obj.getType())).map(obj -> readGroup(areaId, obj));
	}

	protected AreaObject readObject(int areaId, ByteBuffer data) {
		final int start = data.position();

		int typeAndFlags = readByte(data);

		int positionX = readByte(data);
		int positionY = readByte(data);
		int positionZ = readByte(data);

		int sizeX = readByte(data);
		int sizeY = readByte(data);
		int sizeZ = readByte(data);

		int id = readByte(data);
		int byteCount = readByte(data);

		int restCount = byteCount - 9;

		AreaObjectType type = fromId(typeAndFlags & 0xF);
		int flags = (typeAndFlags & 0xF0) >> 4;

		if (AreaObjectType.GROUP.equals(type) || (AreaObjectType.ENTRANCE.equals(type) && id == 255)) {
			final int[] values = new int[restCount + 6];
			values[0] = positionX;
			values[1] = positionY;
			values[2] = positionZ;
			values[3] = sizeX;
			values[4] = sizeY;
			values[5] = sizeZ;
			Array.range(0, restCount).forEach(i -> values[6 + i] = readByte(data));
			return new AreaObject(id, type, flags, values);
		}

		int colorCount = type.getColorCount();
		int colorByteCount = colorCount >> 1;
		int[] colors = new int[colorCount];
		if (colorCount > 0) {
			if (restCount < colorByteCount)
				throw new IllegalStateException("Object " + id + " of type " + type + " has not enough data");

			Array.range(0, colorByteCount).map(i -> readByte(data)).forEachWithIndex((colorData, index) -> {
				colors[2 * index] = colorData & 0x0F;
				colors[2 * index + 1] = (colorData & 0xF0) >> 4;
			});
			restCount -= colorByteCount;
		}

		int ordinatesCount = AreaObjectType.SENSOR.equals(type) ? getSensorOrdCount() : type.getOrdinatesCount();
		int[] ordinates = new int[ordinatesCount];
		if (ordinatesCount > 0) {
			if (restCount < ordinatesCount)
				throw new IllegalStateException("Object " + id + " of type " + type + " has not enough data");

			Array.range(0, ordinatesCount).map(i -> readByte(data)).forEachWithIndex((ordinate, index) -> {
				ordinates[index] = 32 * ordinate;
			});
			restCount -= ordinatesCount;
		}

		FCLScript objectScript = null;
		if (restCount > 0) {
			objectScript = readScript(restCount, data);
		}

		// Correct position in case object script reads after end of object
		data.position(start + byteCount);

		final Vector3i pos = new Vector3i(positionX, positionY, positionZ).mul(32);
		final int sizeFactor = ENTRANCE.equals(type) ? 5 : 32;
		final Vector3i size = new Vector3i(sizeX, sizeY, sizeZ).mul(sizeFactor);

		return new AreaObject(id, type, flags, pos, size, colors, ordinates, objectScript);
	}

	protected GroupHandler readGroup(int areaId, AreaObject obj) {
		final IntBuffer data = IntBuffer.wrap(obj.getOrdinates());

		final int[] controlledObjectIds = new int[3];
		for (int i = 0; i < controlledObjectIds.length; i++)
			controlledObjectIds[i] = readByte(data);

		final Vector3ic offset1 = new Vector3i((byte) readByte(data), (byte) readByte(data), (byte) readByte(data))
			.mul(32);
		final Vector3ic offset2 = new Vector3i((byte) readByte(data), (byte) readByte(data), (byte) readByte(data))
			.mul(32);

		Seq<GroupInstruction> instructions = API.Seq();
		while (data.position() < data.limit()) {
			final int code = readByte(data);
			if ((code & 0x01) > 0) {
				final FCLScript script = readScript(data);
				instructions = instructions.append(new GroupInstructionScript(code, script));
			} else if ((code & 0x80) > 0) {
				instructions = instructions.append(new GroupInstructionRepeat(code));
			} else {
				if (data.position() + 3 > data.limit()) {
					System.err.printf("Reading past group size for group %d in area %d%n", obj.getId(), areaId);
				}
				final int positionX = data.position() < data.limit() ? readByte(data) : 0;
				final int positionY = data.position() < data.limit() ? readByte(data) : 0;
				final int positionZ = data.position() < data.limit() ? readByte(data) : 0;
				final Vector3i pos1 = new Vector3i(positionX, positionY, positionZ).mul(32);
				final Vector3i pos2 = pos1.add(offset1, new Vector3i());
				final Vector3i pos3 = pos2.add(offset2, new Vector3i());
				instructions = instructions.append(new GroupInstructionPosition(code, pos1, pos2, pos3));
			}
		}

		return new GroupHandler(obj, controlledObjectIds, instructions);
	}

	protected FCLScript readScript(Buffer data) {
		int totalLength = readByte(data);
		return readScript(totalLength, data);
	}

	protected FCLScript readScript(int totalLength, Buffer data) {
		int currentLength = 0;
		Seq<FCLInstruction> instSeq = API.Seq();
		while (currentLength < totalLength) {
			FCLInstruction inst = readInstruction(data);
			instSeq = instSeq.append(inst);
			currentLength += inst.getLength();
		}
		if (currentLength != totalLength) {
			System.err.println("script length " + currentLength + " does not match " + totalLength);
		}
		return new FCLScript(instSeq);
	}

	protected FCLInstruction readInstruction(Buffer data) {
		int opcodeAndTrigger = readByte(data);
		FCLOpCode opcode = opcodeFrom(opcodeAndTrigger);
		FCLTrigger trigger = FCLTrigger.fromCode(opcodeAndTrigger & 0xC0);
		int[] args = new int[opcode.getArgumentCount()];
		for (int i = 0; i < args.length; i++) {
			args[i] = readByte(data);
		}
		return new FCLInstruction(1 + opcode.getArgumentCount(), trigger, opcode, args);
	}

	protected static int readByte(Buffer data) {
		if (data instanceof ByteBuffer bbuf)
			return bbuf.get() & 0xFF;
		if (data instanceof IntBuffer ibuf)
			return ibuf.get();
		throw new IllegalArgumentException(data.toString());
	}

	protected static int readShort(ByteBuffer data) {
		return data.getShort() & 0xFFFF;
	}

	protected static void skip(ByteBuffer data, int length) {
		data.position(data.position() + length);
	}

	protected abstract String getAreasFilename();

	protected abstract String getBorderFilename();

	protected abstract String getMessagesFilename();

	protected abstract String getOptionsFilename();

	protected abstract String getTitleFilename();

	protected abstract int getAreasOffset();

	protected abstract int getBorderOffset();

	protected abstract int getMessagesOffset();

	protected abstract int getOptionsOffset();

	protected abstract int getTitleOffset();

	protected abstract int getSensorOrdCount();

	protected abstract MovementMode getDefaultMovement();

	protected abstract Seq<MovementMode> getMovementModes();

	protected abstract Seq<String> readAreaNames(Seq<Message> messages);

	protected abstract String readAreaName(AreaHeader header, ByteBuffer data);

	protected abstract FCLOpCode opcodeFrom(int opCodeAndTrigger);
}
