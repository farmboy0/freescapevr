package freescape;

public record Message(boolean needsOffset, String text) {
}
