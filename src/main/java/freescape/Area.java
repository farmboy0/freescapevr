package freescape;

import java.util.Optional;

import javax.annotation.Nonnull;

import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.collection.SortedMap;
import io.vavr.collection.SortedSet;

import org.joml.RayAabIntersection;
import org.joml.Vector3fc;
import org.joml.Vector3i;
import org.joml.Vector3ic;

import freescape.AreaObject.Face;
import freescape.group.GroupHandler;
import freescape.script.FCLScript;
import math.AABBc;

public class Area {
	public static final int SKY_COLOR_INDEX = 0;
	public static final int GROUND_COLOR_INDEX = 1;
	public static final int BG_NORMAL_COLOR_INDEX = 2;
	public static final int BG_WHEN_HIT_COLOR_INDEX = 3;
	public static final int PAPER_COLOR_INDEX = 4;
	public static final int INK_COLOR_INDEX = 5;

	private static final int GROUND_MIN = -100000;
	private static final int GROUND_MAX = 100000;
	private static final Vector3ic GROUND_A = new Vector3i(GROUND_MIN, 0, GROUND_MIN);
	private static final Vector3ic GROUND_B = new Vector3i(GROUND_MAX, 0, GROUND_MIN);
	private static final Vector3ic GROUND_C = new Vector3i(GROUND_MIN, 0, GROUND_MAX);
	private static final Vector3ic GROUND_D = new Vector3i(GROUND_MAX, 0, GROUND_MAX);

	private final int id;
	private final String name;
	private final int scale;
	private final SortedMap<Integer, AreaObject> entranceMap;
	private final SortedMap<Integer, AreaObject> objectsMap;
	private final Seq<FCLScript> scripts;
	private final Seq<GroupHandler> groups;
	private final Palette palette;
	private final int[] colors;
	private final Face groundFace;

	public Area(int id, @Nonnull String name, int scale, @Nonnull SortedMap<Integer, AreaObject> entranceMap,
		@Nonnull SortedMap<Integer, AreaObject> objectsMap, @Nonnull Seq<FCLScript> scripts,
		@Nonnull Seq<GroupHandler> groups, @Nonnull Palette palette, int... colors) {

		this.id = id;
		this.name = name;
		this.scale = scale;
		this.entranceMap = entranceMap;
		this.objectsMap = objectsMap;
		this.scripts = scripts;
		this.groups = groups;
		this.palette = palette;
		this.colors = colors;
		this.groundFace = new Face(getColor(GROUND_COLOR_INDEX), GROUND_A, GROUND_B, GROUND_C, GROUND_C, GROUND_B,
			GROUND_D);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getScale() {
		return scale;
	}

	public int getColor(int index) {
		if (index >= colors.length) {
			return 0;
		}
		return colors[index];
	}

	@Nonnull
	public Optional<AreaObject> getEntrance(int id) {
		return entranceMap.get(id).toJavaOptional();
	}

	@Nonnull
	public SortedSet<Integer> getEntranceIds() {
		return entranceMap.keySet();
	}

	@Nonnull
	public Face getGroundFace() {
		return groundFace;
	}

	@Nonnull
	public Optional<AreaObject> getObject(int id) {
		return objectsMap.get(id).toJavaOptional();
	}

	@Nonnull
	public Set<AreaObject> getObjects() {
		return objectsMap.values().toSet();
	}

	@Nonnull
	public SortedSet<Integer> getObjectIds() {
		return objectsMap.keySet();
	}

	@Nonnull
	public Palette getPalette() {
		return palette;
	}

	@Nonnull
	public Seq<FCLScript> getScripts() {
		return scripts;
	}

	@Nonnull
	public Seq<GroupHandler> getGroups() {
		return groups;
	}

	@Nonnull
	public Set<AreaObject> getCollisions(@Nonnull AABBc bounds) {
		return getObjects().filter(AreaObject::isLive).filter(obj -> obj.collides(bounds));
	}

	@Nonnull
	public Set<AreaObject> getIntersections(@Nonnull AABBc bounds) {
		return getObjects().filter(AreaObject::isLive).filter(obj -> obj.intersects(bounds));
	}

	@Nonnull
	public Set<AreaObject> getIntersections(@Nonnull Vector3fc omin, @Nonnull Vector3fc omax) {
		return getObjects().filter(AreaObject::isLive).filter(obj -> obj.intersects(omin, omax));
	}

	public Set<AreaObject> getIntersections(@Nonnull RayAabIntersection rayAabIntersection) {
		return getObjects().filter(AreaObject::isLive).filter(obj -> obj.intersects(rayAabIntersection));
	}

	@Override
	public String toString() {
		return name;
	}
}
