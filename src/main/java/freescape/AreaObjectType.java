package freescape;

import io.vavr.API;
import io.vavr.collection.Seq;

import org.joml.Vector3i;
import org.joml.Vector3ic;

import freescape.AreaObject.Face;

public enum AreaObjectType {
	ENTRANCE(0, 0, AreaObjectTypeClass.NONE, (p, s, o, c) -> API.Seq()), //
	CUBE(6, 0, AreaObjectTypeClass.NONE, AreaObjectType::cubeFaces), //
	SENSOR(0, 0, AreaObjectTypeClass.NONE, AreaObjectType::sensorFaces), //
	RECTANGLE(2, 0, AreaObjectTypeClass.NONE, AreaObjectType::rectFaces), //

	EASTPYRAMID(6, 4, AreaObjectTypeClass.PYRAMID, AreaObjectType::eastPyFaces), //
	WESTPYRAMID(6, 4, AreaObjectTypeClass.PYRAMID, AreaObjectType::westPyFaces), //
	UPPYRAMID(6, 4, AreaObjectTypeClass.PYRAMID, AreaObjectType::upPyFaces), //
	DOWNPYRAMID(6, 4, AreaObjectTypeClass.PYRAMID, AreaObjectType::downPyFaces), //
	NORTHPYRAMID(6, 4, AreaObjectTypeClass.PYRAMID, AreaObjectType::northPyFaces), //
	SOUTHPYRAMID(6, 4, AreaObjectTypeClass.PYRAMID, AreaObjectType::southPyFaces), //

	LINE(2, 6, AreaObjectTypeClass.POLYGON, AreaObjectType::polyFaces), //
	TRIANGLE(2, 9, AreaObjectTypeClass.POLYGON, AreaObjectType::polyFaces), //
	QUADRILATERAL(2, 12, AreaObjectTypeClass.POLYGON, AreaObjectType::polyFaces), //
	PENTAGON(2, 15, AreaObjectTypeClass.POLYGON, AreaObjectType::polyFaces), //
	HEXAGON(2, 18, AreaObjectTypeClass.POLYGON, AreaObjectType::polyFaces), //

	GROUP(0, 0, AreaObjectTypeClass.NONE, (p, s, o, c) -> API.Seq()), //
	;

	private final int colorCount;
	private final int ordinatesCount;
	private final AreaObjectTypeClass type;
	private final FacesCreator facesCreator;

	private AreaObjectType(int colorCount, int ordinatesCount, AreaObjectTypeClass type, FacesCreator facesCreator) {
		this.colorCount = colorCount;
		this.ordinatesCount = ordinatesCount;
		this.type = type;
		this.facesCreator = facesCreator;
	}

	public Seq<Face> createFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		return facesCreator.createFrom(pos, size, ordinates, colors);
	}

	public int getColorCount() {
		return colorCount;
	}

	public int getOrdinatesCount() {
		return ordinatesCount;
	}

	public boolean isPlanar() {
		return AreaObjectTypeClass.POLYGON.equals(type) || AreaObjectType.RECTANGLE.equals(this);
	}

	public boolean isPolygon() {
		return AreaObjectTypeClass.POLYGON.equals(type);
	}

	public boolean isPyramid() {
		return AreaObjectTypeClass.PYRAMID.equals(type);
	}

	public static AreaObjectType fromId(int id) {
		return API.Seq(values()).find(type -> type.ordinal() == id).getOrElseThrow(IllegalArgumentException::new);
	}

	private enum AreaObjectTypeClass {
		NONE, POLYGON, PYRAMID;
	}

	private interface FacesCreator {
		Seq<Face> createFrom(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors);
	}

	private static Seq<Face> cubeFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		final Vector3ic v0 = negX(pos);
		final Vector3ic v1 = negX(addX(pos, size));
		final Vector3ic v2 = negX(addY(pos, size));
		final Vector3ic v3 = negX(addXY(pos, size));
		final Vector3ic v4 = negX(addZ(pos, size));
		final Vector3ic v5 = negX(addXZ(pos, size));
		final Vector3ic v6 = negX(addYZ(pos, size));
		final Vector3ic v7 = negX(add(pos, size));
		return cube(v0, v1, v2, v3, v4, v5, v6, v7, colors);
	}

	private static Seq<Face> sensorFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		return API.Seq();
	}

	/**
	 * <pre>
	 *  c ________ d
	 *   |        |
	 *   |        |
	 *   |        |
	 *   |        |
	 *   |________|
	 *  a          b
	 * </pre>
	 */
	private static Seq<Face> rectFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		assert (size.x() == 0 || size.y() == 0 || size.z() == 0);

		final int dx = size.y() == 0 || size.z() == 0 ? size.x() : 0;
		final int dz = size.x() == 0 || size.y() == 0 ? size.z() : 0;

		final Vector3ic a = negX(pos);
		final Vector3ic b = negX(add(pos, dx, size.x() == 0 ? size.y() : 0, 0));
		final Vector3ic c = negX(add(pos, 0, size.z() == 0 ? size.y() : 0, dz));
		final Vector3ic d = negX(add(pos, size));

		return quad(a, b, c, d, colors[0], colors[1]);
	}

	// Pyramid base is the left side of a cube(west pyramid rotated 180deg around Y)
	private static Seq<Face> eastPyFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		final Vector3ic v0 = negX(addZ(pos, size));
		final Vector3ic v1 = negX(addYZ(pos, size));
		final Vector3ic v2 = negX(pos);
		final Vector3ic v3 = negX(addY(pos, size));
		final Vector3ic v4 = negX(add(pos, size.x(), ordinates[0], ordinates[3]));
		final Vector3ic v5 = negX(add(pos, size.x(), ordinates[2], ordinates[3]));
		final Vector3ic v6 = negX(add(pos, size.x(), ordinates[0], ordinates[1]));
		final Vector3ic v7 = negX(add(pos, size.x(), ordinates[2], ordinates[1]));
		return pyramid(v0, v1, v2, v3, v4, v5, v6, v7, colors);
	}

	// Pyramid base is the right side of a cube(up pyramid rotated to the left around Z)
	private static Seq<Face> westPyFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		final Vector3ic v0 = negX(addX(pos, size));
		final Vector3ic v1 = negX(addXY(pos, size));
		final Vector3ic v2 = negX(addXZ(pos, size));
		final Vector3ic v3 = negX(add(pos, size));
		final Vector3ic v4 = negX(add(pos, 0, ordinates[0], ordinates[1]));
		final Vector3ic v5 = negX(add(pos, 0, ordinates[2], ordinates[1]));
		final Vector3ic v6 = negX(add(pos, 0, ordinates[0], ordinates[3]));
		final Vector3ic v7 = negX(add(pos, 0, ordinates[2], ordinates[3]));
		return pyramid(v0, v1, v2, v3, v4, v5, v6, v7, colors);
	}

	// Pyramid base is the bottom side of a cube
	private static Seq<Face> upPyFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		final Vector3ic v0 = negX(pos);
		final Vector3ic v1 = negX(addX(pos, size));
		final Vector3ic v2 = negX(addZ(pos, size));
		final Vector3ic v3 = negX(addXZ(pos, size));
		final Vector3ic v4 = negX(add(pos, ordinates[0], size.y(), ordinates[1]));
		final Vector3ic v5 = negX(add(pos, ordinates[2], size.y(), ordinates[1]));
		final Vector3ic v6 = negX(add(pos, ordinates[0], size.y(), ordinates[3]));
		final Vector3ic v7 = negX(add(pos, ordinates[2], size.y(), ordinates[3]));
		return pyramid(v0, v1, v2, v3, v4, v5, v6, v7, colors);
	}

	// Pyramid base is the top side of a cube(up pyramid rotated 180deg around Z)
	private static Seq<Face> downPyFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		final Vector3ic v0 = negX(addXY(pos, size));
		final Vector3ic v1 = negX(addY(pos, size));
		final Vector3ic v2 = negX(add(pos, size));
		final Vector3ic v3 = negX(addYZ(pos, size));
		final Vector3ic v4 = negX(add(pos, ordinates[2], 0, ordinates[1]));
		final Vector3ic v5 = negX(add(pos, ordinates[0], 0, ordinates[1]));
		final Vector3ic v6 = negX(add(pos, ordinates[2], 0, ordinates[3]));
		final Vector3ic v7 = negX(add(pos, ordinates[0], 0, ordinates[3]));
		return pyramid(v0, v1, v2, v3, v4, v5, v6, v7, colors);
	}

	// Pyramid base is the front side of a cube(up pyramid rotated to the back around X)
	private static Seq<Face> northPyFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		final Vector3ic v0 = negX(addY(pos, size));
		final Vector3ic v1 = negX(addXY(pos, size));
		final Vector3ic v2 = negX(pos);
		final Vector3ic v3 = negX(addX(pos, size));
		final Vector3ic v4 = negX(add(pos, ordinates[0], ordinates[3], size.z()));
		final Vector3ic v5 = negX(add(pos, ordinates[2], ordinates[3], size.z()));
		final Vector3ic v6 = negX(add(pos, ordinates[0], ordinates[1], size.z()));
		final Vector3ic v7 = negX(add(pos, ordinates[2], ordinates[1], size.z()));
		return pyramid(v0, v1, v2, v3, v4, v5, v6, v7, colors);
	}

	// Pyramid base is the back side of a cube(up pyramid rotated to the front around X)
	private static Seq<Face> southPyFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		final Vector3ic v0 = negX(addZ(pos, size));
		final Vector3ic v1 = negX(addXZ(pos, size));
		final Vector3ic v2 = negX(addYZ(pos, size));
		final Vector3ic v3 = negX(add(pos, size));
		final Vector3ic v4 = negX(add(pos, ordinates[0], ordinates[1], 0));
		final Vector3ic v5 = negX(add(pos, ordinates[2], ordinates[1], 0));
		final Vector3ic v6 = negX(add(pos, ordinates[0], ordinates[3], 0));
		final Vector3ic v7 = negX(add(pos, ordinates[2], ordinates[3], 0));
		return pyramid(v0, v1, v2, v3, v4, v5, v6, v7, colors);
	}

	private static Seq<Face> polyFaces(Vector3ic pos, Vector3ic size, int[] ordinates, int[] colors) {
		final Vector3ic[] vertices = new Vector3ic[ordinates.length / 3];
		for (int i = 0; i < vertices.length; i++) {
			vertices[i] = negX(new Vector3i(ordinates[3 * i], ordinates[3 * i + 1], ordinates[3 * i + 2]));
		}
		final Vector3ic v0 = vertices[0];
		final Seq<Vector3ic> triangles = API.Seq(vertices)
			.drop(1)
			.sliding(2, 1)
			.flatMap(seq -> seq.size() == 2 || vertices.length == 2 ? seq.prepend(v0) : API.Seq())
			.toArray();
		return API.Seq(new Face(colors[0], triangles), new Face(colors[1], triangles.reverse()));
	}

	private static Vector3ic add(Vector3ic v1, int x, int y, int z) {
		return v1.add(x, y, z, new Vector3i());
	}

	private static Vector3ic add(Vector3ic v1, Vector3ic v2) {
		return v1.add(v2, new Vector3i());
	}

	private static Vector3ic addX(Vector3ic v1, Vector3ic v2) {
		return v1.add(v2.x(), 0, 0, new Vector3i());
	}

	private static Vector3ic addY(Vector3ic v1, Vector3ic v2) {
		return v1.add(0, v2.y(), 0, new Vector3i());
	}

	private static Vector3ic addZ(Vector3ic v1, Vector3ic v2) {
		return v1.add(0, 0, v2.z(), new Vector3i());
	}

	private static Vector3ic addXY(Vector3ic v1, Vector3ic v2) {
		return v1.add(v2.x(), v2.y(), 0, new Vector3i());
	}

	private static Vector3ic addXZ(Vector3ic v1, Vector3ic v2) {
		return v1.add(v2.x(), 0, v2.z(), new Vector3i());
	}

	private static Vector3ic addYZ(Vector3ic v1, Vector3ic v2) {
		return v1.add(0, v2.y(), v2.z(), new Vector3i());
	}

	private static Seq<Face> quad(Vector3ic a, Vector3ic b, Vector3ic c, Vector3ic d, int color) {
		return quad(a, b, c, d, color, color);
	}

	private static Seq<Face> quad(Vector3ic a, Vector3ic b, Vector3ic c, Vector3ic d, int color1, int color2) {
		return API.Seq(new Face(color1, a, b, c, c, b, d), new Face(color2, b, a, d, d, a, c));
	}

	/**
	 * <pre>
	 *     v6_________ v7
	 *      /        /|
	 *     / |      / |
	 *    /        /  |
	 * v2/___|____/v3 |
	 *   |    _ _ | _ |
	 *   | v4/    |   /v5
	 *   |        |  /
	 *   | /      | /
	 *   | _______|/
	 *   v0       v1
	 * </pre>
	 */
	private static Seq<Face> cube(Vector3ic v0, Vector3ic v1, Vector3ic v2, Vector3ic v3, Vector3ic v4, Vector3ic v5,
		Vector3ic v6, Vector3ic v7, int[] colors) {

		final Seq<Face> left = quad(v4, v0, v6, v2, colors[0]);
		final Seq<Face> right = quad(v1, v5, v3, v7, colors[1]);
		final Seq<Face> bottom = quad(v4, v5, v0, v1, colors[2]);
		final Seq<Face> top = quad(v2, v3, v6, v7, colors[3]);
		final Seq<Face> front = quad(v0, v1, v2, v3, colors[4]);
		final Seq<Face> back = quad(v5, v4, v7, v6, colors[5]);
		return left.appendAll(right).appendAll(bottom).appendAll(top).appendAll(front).appendAll(back);
	}

	/**
	 * <pre>
	 *      v6 ___ v7
	 *        /| /\
	 *     v4/__/v5\
	 *      |v2_| _ \ v3
	 *     /  / \    |
	 *    |      |   /
	 *    / /    \  /
	 *   | _______|/
	 *   v0       v1
	 * </pre>
	 */
	private static Seq<Face> pyramid(Vector3ic v0, Vector3ic v1, Vector3ic v2, Vector3ic v3, Vector3ic v4, Vector3ic v5,
		Vector3ic v6, Vector3ic v7, int[] colors) {

		final Seq<Face> front = quad(v0, v1, v4, v5, colors[0]);
		final Seq<Face> right = quad(v1, v3, v5, v7, colors[1]);
		final Seq<Face> back = quad(v3, v2, v7, v6, colors[2]);
		final Seq<Face> left = quad(v2, v0, v6, v4, colors[3]);
		final Seq<Face> bottom = quad(v0, v1, v2, v3, colors[4]);
		final Seq<Face> top = quad(v4, v5, v6, v7, colors[5]);
		return front.appendAll(right).appendAll(back).appendAll(left).appendAll(bottom).appendAll(top);
	}

	private static Vector3ic negX(Vector3ic v) {
		return new Vector3i(v).setComponent(0, -v.x());
	}
}
