package freescape.group;

import javax.annotation.Nonnull;

import org.joml.Vector3ic;

import freescape.AreaObject;
import freescape.script.FCLGameState;

public class GroupInstructionPosition extends GroupInstruction {

	private final Vector3ic[] positions;

	public GroupInstructionPosition(int code, @Nonnull Vector3ic pos1, @Nonnull Vector3ic pos2,
		@Nonnull Vector3ic pos3) {

		super(code);
		this.positions = new Vector3ic[] { pos1, pos2, pos3 };
	}

	public Vector3ic getPos1() {
		return positions[0];
	}

	public Vector3ic getPos2() {
		return positions[1];
	}

	public Vector3ic getPos3() {
		return positions[2];
	}

	@Override
	public boolean execute(@Nonnull GroupHandler handler, @Nonnull FCLGameState state) {
		if (isDestroyGroup()) {
			handler.getGroup().destroy();
		}

		boolean canContinue = true;
		if (isConditional()) {
			if (!handler.isCanContinue())
				canContinue = false;
			else
				handler.setCanContinue(false);
		}

		if (handler.getControlledObjectId1() == 0) {
			System.err.println("error in group " + handler.getGroup().getId());
			return canContinue;
		}

		final AreaObject obj1 = state.getObject(handler.getControlledObjectId1()).orElseThrow();
		execute(obj1, getPos1());

		if (handler.getControlledObjectId2() != 0) {
			final AreaObject obj2 = state.getObject(handler.getControlledObjectId2()).orElseThrow();
			execute(obj2, getPos2());

			if (handler.getControlledObjectId3() != 0) {
				final AreaObject obj3 = state.getObject(handler.getControlledObjectId3()).orElseThrow();
				execute(obj3, getPos3());
			}
		}

		return canContinue;
	}

	private void execute(@Nonnull AreaObject controlledObject, Vector3ic position) {
		if (isUpdateFlags()) {
			controlledObject.setVisible();
			if (isSetFlagDestroyed()) {
				controlledObject.setDestroyed(true);
			}
			if (isSetFlagInvisible()) {
				controlledObject.setInvisible();
			}
		}
		controlledObject.moveTo(position);
		if (!controlledObject.isInvisible()) {
			controlledObject.setFlag1(true);
		}
	}

	@Override
	public String toString() {
		return String.format("POS(0x%02X,<%d,%d,%d>,<%d,%d,%d>,<%d,%d,%d>)", getCode(), getPos1().x(), getPos1().y(),
			getPos1().z(), getPos2().x(), getPos2().y(), getPos2().z(), getPos3().x(), getPos3().y(), getPos3().z());
	}
}
