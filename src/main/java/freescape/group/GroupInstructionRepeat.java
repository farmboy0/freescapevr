package freescape.group;

import javax.annotation.Nonnull;

import freescape.script.FCLGameState;

public class GroupInstructionRepeat extends GroupInstruction {

	public GroupInstructionRepeat(int code) {
		super(code);
	}

	@Override
	public boolean execute(@Nonnull GroupHandler group, @Nonnull FCLGameState state) {
		group.rewind();
		return false;
	}

	@Override
	public String toString() {
		return String.format("REPEAT(0x%02X)", getCode());
	}
}
