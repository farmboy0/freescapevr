package freescape.group;

import javax.annotation.Nonnull;

import freescape.script.FCLGameState;

public abstract class GroupInstruction {

	private final int code;

	protected GroupInstruction(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public boolean isDestroyGroup() {
		return (getCode() & 0x04) > 0;
	}

	public boolean isUpdateFlags() {
		return (getCode() & 0x08) > 0;
	}

	public boolean isConditional() {
		return (getCode() & 0x10) > 0;
	}

	public boolean isSetFlagDestroyed() {
		return (getCode() & 0x20) > 0;
	}

	public boolean isSetFlagInvisible() {
		return (getCode() & 0x40) > 0;
	}

	public abstract boolean execute(@Nonnull GroupHandler group, @Nonnull FCLGameState state);
}
