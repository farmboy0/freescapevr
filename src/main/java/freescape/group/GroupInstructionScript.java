package freescape.group;

import javax.annotation.Nonnull;

import freescape.script.FCLGameState;
import freescape.script.FCLScript;

public class GroupInstructionScript extends GroupInstruction {

	private final FCLScript script;

	public GroupInstructionScript(int code, @Nonnull FCLScript script) {
		super(code);
		this.script = script;
	}

	public FCLScript getScript() {
		return script;
	}

	@Override
	public boolean execute(GroupHandler group, @Nonnull FCLGameState state) {
		getScript().exec(null, state);
		return true;
	}

	@Override
	public String toString() {
		return getScript().toString();
	}
}
