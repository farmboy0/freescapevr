package freescape.group;

import javax.annotation.Nonnull;

import io.vavr.collection.Seq;

import freescape.AreaObject;
import freescape.script.FCLGameState;

public class GroupHandler {

	private final AreaObject group;
	private final int[] controlledObjectIds;
	private final Seq<GroupInstruction> instructions;

	private int currentIdx;
	private boolean canContinue;

	public GroupHandler(@Nonnull AreaObject group, @Nonnull int[] controlledObjectIds,
		@Nonnull Seq<GroupInstruction> instructions) {

		this.group = group;
		this.controlledObjectIds = controlledObjectIds;
		this.instructions = instructions;
	}

	public AreaObject getGroup() {
		return group;
	}

	public int getControlledObjectId1() {
		return controlledObjectIds[0];
	}

	public int getControlledObjectId2() {
		return controlledObjectIds[1];
	}

	public int getControlledObjectId3() {
		return controlledObjectIds[2];
	}

	public Seq<GroupInstruction> getInstructions() {
		return instructions;
	}

	public boolean isCanContinue() {
		return canContinue;
	}

	public boolean isRunnable() {
		return !isEnded() && !group.isDestroyed();
	}

	public boolean isEnded() {
		return currentIdx >= instructions.size();
	}

	public void rewind() {
		currentIdx = 0;
	}

	public void setCanContinue(boolean canContinue) {
		this.canContinue = canContinue;
	}

	public void update(@Nonnull FCLGameState state) {
		GroupInstruction inst;
		do {
			inst = instructions.get(currentIdx);
			if (inst.execute(this, state)) {
				currentIdx++;
			}
		} while (!(inst instanceof GroupInstructionPosition));
	}
}
