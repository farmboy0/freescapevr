package freescape;

public enum AreaObjectFlag {
	UNKNOWN(0x1), //
	DESTROYED(0x2), //
	INVISIBLE(0x4), //
	INVISIBLE_BY_DEFAULT(0x8), //
	;

	private int bitmask;

	AreaObjectFlag(int bitmask) {
		this.bitmask = bitmask;
	}

	public boolean getFrom(int flags) {
		return (flags & bitmask) > 0;
	}

	public int setTo(int flags, boolean value) {
		if (value) {
			return flags | bitmask;
		}
		return flags & ~bitmask;
	}

	public int toggle(int flags) {
		return setTo(flags, !getFrom(flags));
	}
}
