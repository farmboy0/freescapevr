package freescape;

import java.awt.Color;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Seq;

public class Palette {
	public static final Seq<Color> EGA_PALETTE = API.Seq( //
		Color.black, // black
		new Color(0, 0, 0xAA), // blue
		new Color(0, 0xAA, 0), // green
		new Color(0, 0xAA, 0xAA), // cyan
		new Color(0xAA, 0, 0), // red
		new Color(0xAA, 0, 0xAA), // magenta
		new Color(0xAA, 0x55, 0), // brown
		new Color(0xAA, 0xAA, 0xAA), // light gray
		new Color(0x55, 0x55, 0x55), // dark gray
		new Color(0x55, 0x55, 0xFF), // bright blue
		new Color(0x55, 0xFF, 0x55), // bright green
		new Color(0x55, 0xFF, 0xFF), // bright cyan
		new Color(0xFF, 0x55, 0x55), // bright red
		new Color(0xFF, 0x55, 0xFF), // bright magenta
		new Color(0xFF, 0xFF, 0x55), // bright yellow
		Color.white // white
	);

	protected final int[] colorMap;

	public Palette(@Nonnull int[] colorMap) {
		this.colorMap = colorMap;
	}

	@CheckForNull
	public Color getColor(int idx) {
		if (colorMap[idx] < 0 || colorMap[idx] > 15) {
			return null;
		}
		return EGA_PALETTE.get(colorMap[idx]);
	}

	@CheckForNull
	public Color getColor1(int idx) {
		if (colorMap[idx] < 0 || colorMap[idx] > 15) {
			return null;
		}
		return EGA_PALETTE.get(colorMap[idx]);
	}

	@CheckForNull
	public Color getColor2(int idx) {
		return null;
	}

	public boolean isStipple(int idx) {
		return false;
	}
}
