package freescape.render;

import org.lwjgl.opengl.GL11;

import freescape.AreaObject;
import freescape.Palette;

public class PolygonRenderer implements AreaObjectRenderer {

	@Override
	public void render(AreaObject obj, Palette palette) {
		GL11.glEnable(GL11.GL_POLYGON_OFFSET_FILL);
		GL11.glPolygonOffset(-2.0f, 1.f);

		obj.getFaces().forEach(face -> render(face, palette));

		GL11.glPolygonOffset(0, 0);
		GL11.glDisable(GL11.GL_POLYGON_OFFSET_FILL);
	}
}
