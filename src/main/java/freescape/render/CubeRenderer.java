package freescape.render;

import freescape.AreaObject;
import freescape.Palette;

public class CubeRenderer implements AreaObjectRenderer {

	@Override
	public void render(AreaObject obj, Palette palette) {
		obj.getFaces().forEach(face -> render(face, palette));
	}
}
