package freescape.render;

import java.awt.Color;

import javax.annotation.Nonnull;

import org.lwjgl.opengl.GL11;

import freescape.AreaObject;
import freescape.AreaObject.Face;
import freescape.Palette;

public interface AreaObjectRenderer {
	void render(@Nonnull AreaObject obj, @Nonnull Palette palette);

	default void render(@Nonnull Face face, @Nonnull Palette palette) {
		AreaObjectRenderer.renderFace(face, palette);
	}

	static void renderFace(@Nonnull Face face, @Nonnull Palette palette) {
		final Color c = palette.getColor(face.color());
		if (c == null)
			return;

		color(c);

		GL11.glBegin(face.isLine() ? GL11.GL_LINES : GL11.GL_TRIANGLES);
		face.vertices().forEach(v -> GL11.glVertex3d(v.x(), v.y(), v.z()));
		GL11.glEnd();
	}

	static void color(@Nonnull Color c) {
		GL11.glColor4f(c.getRed() / 255.0f, c.getGreen() / 255.0f, c.getBlue() / 255.0f, 1.0f);
	}
}
