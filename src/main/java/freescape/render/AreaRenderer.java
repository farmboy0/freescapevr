package freescape.render;

import static io.vavr.collection.HashMap.ofEntries;
import static io.vavr.collection.Map.entry;

import java.awt.Color;
import java.util.Comparator;

import io.vavr.collection.Map;
import io.vavr.collection.Set;

import org.lwjgl.opengl.GL11;

import engine.AreaProvider;
import freescape.Area;
import freescape.AreaObject;
import freescape.AreaObjectType;
import freescape.Palette;

public class AreaRenderer {
	private static final AreaObjectRenderer NULL_RENDERER = (o, p) -> {
	};

	private static final AreaObjectRenderer CUBE_RENDERER = new CubeRenderer();
	private static final AreaObjectRenderer POLY_RENDERER = new PolygonRenderer();

	private static final Map<AreaObjectType, AreaObjectRenderer> RENDERERS = ofEntries( //
		entry(AreaObjectType.RECTANGLE, POLY_RENDERER), //
		entry(AreaObjectType.LINE, POLY_RENDERER), //
		entry(AreaObjectType.TRIANGLE, POLY_RENDERER), //
		entry(AreaObjectType.QUADRILATERAL, POLY_RENDERER), //
		entry(AreaObjectType.PENTAGON, POLY_RENDERER), //
		entry(AreaObjectType.HEXAGON, POLY_RENDERER), //
		entry(AreaObjectType.CUBE, CUBE_RENDERER), //
		entry(AreaObjectType.EASTPYRAMID, CUBE_RENDERER), //
		entry(AreaObjectType.WESTPYRAMID, CUBE_RENDERER), //
		entry(AreaObjectType.UPPYRAMID, CUBE_RENDERER), //
		entry(AreaObjectType.DOWNPYRAMID, CUBE_RENDERER), //
		entry(AreaObjectType.NORTHPYRAMID, CUBE_RENDERER), //
		entry(AreaObjectType.SOUTHPYRAMID, CUBE_RENDERER) //
	);

	public void render(AreaProvider areaProvider) {
		final Area area = areaProvider.getCurrentArea();
		final Set<AreaObject> objects = area.getObjects().addAll(areaProvider.getSharedArea().getObjects());
		renderSky(area);
		renderFloor(area);
		objects.filter(AreaObject::isLive)
			.filter(obj -> !obj.getType().isPlanar())
			.toSortedSet(Comparator.comparing(AreaObject::getId))
			.forEach(o -> renderObject(o, area.getPalette()));
		objects.filter(AreaObject::isLive)
			.filter(obj -> obj.getType().isPlanar())
			.toSortedSet(Comparator.comparing(AreaObject::getId))
			.forEach(o -> renderObject(o, area.getPalette()));
	}

	private void renderObject(AreaObject obj, Palette palette) {
		RENDERERS.get(obj.getType()).getOrElse(NULL_RENDERER).render(obj, palette);
	}

	private void renderSky(Area area) {
		renderSky(area, Area.BG_NORMAL_COLOR_INDEX);
		renderSky(area, Area.SKY_COLOR_INDEX);
	}

	private void renderSky(Area area, int colorIndex) {
		final int skyColorIndex = area.getColor(colorIndex);
		final Color c = area.getPalette().getColor(skyColorIndex);
		if (c == null)
			return;
		GL11.glClearColor(c.getRed() / 255.0f, c.getGreen() / 255.0f, c.getBlue() / 255.0f, 1.0f);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
	}

	private void renderFloor(Area area) {
		AreaObjectRenderer.renderFace(area.getGroundFace(), area.getPalette());
	}
}
