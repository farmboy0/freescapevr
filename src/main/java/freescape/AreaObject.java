package freescape;

import java.util.Arrays;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import io.vavr.API;
import io.vavr.collection.Seq;

import org.joml.RayAabIntersection;
import org.joml.Vector3f;
import org.joml.Vector3fc;
import org.joml.Vector3i;
import org.joml.Vector3ic;

import freescape.script.FCLScript;
import math.AABB;
import math.AABBc;

public class AreaObject {
	private final int id;
	private final AreaObjectType type;
	private int flags;
	private Vector3ic pos;
	private Vector3ic size;
	private final int[] colors;
	private final int[] ordinates;
	private final FCLScript script;
	private Seq<Face> faces;
	private AABB bounds;

	public AreaObject(int id, @Nonnull AreaObjectType type, int flags, @Nonnull int[] ordinates) {
		this(id, type, flags, null, null, null, ordinates, null);
	}

	public AreaObject(int id, @Nonnull AreaObjectType type, int flags, @Nullable Vector3ic pos,
		@Nullable Vector3ic size, @Nullable int[] colors, @Nonnull int[] ordinates, @Nullable FCLScript script) {

		this.id = id;
		this.type = type;
		this.flags = flags;
		this.pos = pos;
		this.size = size;
		this.colors = colors;
		this.ordinates = ordinates;
		this.script = script;
		this.faces = type.createFaces(pos, size, ordinates, colors);
		this.bounds = new AABB(faces.flatMap(Face::vertices).map(Vector3f::new));
		reset();
	}

	private final Vector3i posOffset = new Vector3i();

	public void destroy() {
		setDestroyed(true);
		setInvisible();
	}

	public void hide() {
		setVisibility(true);
	}

	public void show() {
		setVisibility(false);
	}

	public void moveTo(@Nonnull Vector3ic pos) {
		if (type.isPolygon()) {
			pos.sub(this.pos, posOffset);
			for (int i = 0; i < ordinates.length; i += 3) {
				ordinates[i + 0] += posOffset.x();
				ordinates[i + 1] += posOffset.y();
				ordinates[i + 2] += posOffset.z();
			}
		}
		this.pos = pos;
		this.faces = type.createFaces(pos, size, ordinates, colors);
		this.bounds = new AABB(faces.flatMap(Face::vertices).map(Vector3f::new));
	}

	public void reset() {
		setFlag1(false);
		setDestroyed(false);
		setVisibility(isDefaultInvisible());
	}

	public int getId() {
		return id;
	}

	@Nonnull
	public AreaObjectType getType() {
		return type;
	}

	public int getFlags() {
		return flags;
	}

	public Vector3ic getPos() {
		return pos;
	}

	public Vector3ic getSize() {
		return size;
	}

	public int getColor(int idx) {
		return colors[idx];
	}

	public int[] getOrdinates() {
		return ordinates;
	}

	@CheckForNull
	public FCLScript getScript() {
		return script;
	}

	@Nonnull
	public Seq<Face> getFaces() {
		return faces;
	}

	@Nonnull
	public AABBc getBounds() {
		return bounds;
	}

	public float distance(@Nonnull AABBc other) {
		return bounds.distance(other);
	}

	public boolean collides(@Nonnull AABBc other) {
		return bounds.collides(other);
	}

	public boolean intersects(@Nonnull AABBc other) {
		return bounds.intersects(other);
	}

	public boolean intersects(@Nonnull Vector3fc omin, @Nonnull Vector3fc omax) {
		return bounds.intersects(omin, omax);
	}

	public boolean intersects(@Nonnull RayAabIntersection rayAabIntersection) {
		return bounds.intersects(rayAabIntersection);
	}

	public boolean isDestroyed() {
		return AreaObjectFlag.DESTROYED.getFrom(flags);
	}

	public boolean isInvisible() {
		return AreaObjectFlag.INVISIBLE.getFrom(flags);
	}

	public boolean isDefaultInvisible() {
		return AreaObjectFlag.INVISIBLE_BY_DEFAULT.getFrom(flags);
	}

	public boolean isVisible() {
		return !isInvisible();
	}

	public boolean isLive() {
		return !isDestroyed() && !isInvisible();
	}

	public void setFlag1(boolean flag1) {
		this.flags = AreaObjectFlag.UNKNOWN.setTo(flags, flag1);
	}

	public void setDestroyed(boolean destroyed) {
		this.flags = AreaObjectFlag.DESTROYED.setTo(flags, destroyed);
	}

	public void setVisibility(boolean invisible) {
		this.flags = AreaObjectFlag.INVISIBLE.setTo(flags, invisible);
	}

	public void setInvisible() {
		setVisibility(true);
	}

	public void setVisible() {
		setVisibility(false);
	}

	public void toggleVisibility() {
		this.flags = AreaObjectFlag.INVISIBLE.toggle(flags);
	}

	public int upperYBound() {
		return pos.y() + size.y();
	}

	@Override
	public String toString() {
		return String.format(
			"AreaObject [id=0x%02X, type=%s, flags=0x%02X, pos=%s, size=%s, colors=%s, ordinates=%s, faces=%s]", id,
			type, flags, toString(pos), toString(size), Arrays.toString(colors), Arrays.toString(ordinates),
			faces.mkString(":"));
	}

	public String toShortString() {
		return String.format("AreaObject [id=0x%02X, type=%s, flags=0x%02X]", id, type, flags);
	}

	public record Face(int color, Seq<Vector3ic> vertices) {
		public Face(int color, Vector3ic... vertices) {
			this(color, API.Seq(vertices));
		}

		public boolean isLine() {
			return vertices.size() == 2;
		}

		@Override
		public String toString() {
			return String.format("[color=%d, verts=%s]", color, vertices.map(AreaObject::toString).mkString(","));
		}
	}

	private static String toString(Vector3ic v) {
		if (v == null)
			return "";
		return String.format("<%d,%d,%d>", v.x(), v.y(), v.z());
	}
}
