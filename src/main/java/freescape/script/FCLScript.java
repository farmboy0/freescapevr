package freescape.script;

import java.util.function.BiConsumer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import io.vavr.collection.Seq;

public class FCLScript {

	static final BiConsumer<FCLGameState, FCLInstruction> DUMMY = (state, inst) -> {
	};

	private final Seq<FCLInstruction> code;

	public FCLScript(@Nonnull Seq<FCLInstruction> code) {
		this.code = code;
	}

	public void exec(@Nullable FCLTrigger trigger, @Nonnull FCLGameState state) {
		final Seq<FCLInstruction> script = code.filter(inst -> inst.matches(trigger));
		if (!script.isEmpty()) {
			FCLScriptState scriptState = new FCLScriptState(trigger);
			System.out.println("------------------------");
			while (scriptState.instIndex < script.length()) {
				final FCLInstruction inst = script.get(scriptState.instIndex);
				inst.getOpcode().exec(state, inst, scriptState);
				scriptState.instIndex++;
			}
		}
	}

	@Override
	public String toString() {
		return code.mkString("\n");
	}
}
