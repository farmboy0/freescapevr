package freescape.script;

import java.util.Optional;

import javax.annotation.Nullable;

public class FCLScriptState {
	public boolean execute;
	public int instIndex;
	public int ifsCount;
	public int ifsTaken;
	public int loopCount;
	public int loopStart;

	public final Optional<FCLTrigger> trigger;

	public FCLScriptState(@Nullable FCLTrigger trigger) {
		this.execute = true;
		this.instIndex = 0;
		this.ifsCount = 0;
		this.ifsTaken = 0;
		this.trigger = Optional.ofNullable(trigger);
	}
}
