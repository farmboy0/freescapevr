package freescape.script;

import io.vavr.API;

public enum FCLTrigger {
	COLLISION(0x00), //
	TIMER(0x40), //
	SHOOTING(0x80), //
	INTERACTION(0xC0), //
	;

	private final int code;

	private FCLTrigger(int code) {
		this.code = code;
	}

	public static FCLTrigger fromCode(int code) {
		return API.Seq(values())
			.find(trg -> trg.getCode() == code)
			.getOrElseThrow(() -> new IllegalArgumentException("Unknown trigger code " + code));
	}

	public int getCode() {
		return code;
	}
}
