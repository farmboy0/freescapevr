package freescape.script;

import java.util.Optional;

import freescape.AreaObject;

public interface FCLGameState {

	void setFalse(int booleanId);

	void setTrue(int booleanId);

	void toggleBoolean(int booleanId);

	void changeStrength(int strength);

	boolean getBoolean(int booleanId);

	void triggerAnimation(int objectId);

	Optional<AreaObject> getObject(int objectId);

	Optional<AreaObject> getObject(int areaId, int objectId);

	int getVariable(int variableId);

	void increaseScore(int score1, int score2, int score3);

	void moveToArea(int areaId, int entranceId);

	void pause(int length);

	void playSfx(int sfxId);

	void playSfxNextFrame(int sfxId);

	void showEffect(int code);

	void showMessage(int msgId);

	void decrement(int variableId);

	void increment(int variableId);

	void setVariable(int variableId, int value);

}
