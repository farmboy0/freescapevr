package freescape.script;

import static freescape.script.FCLScript.DUMMY;

import java.util.function.BiConsumer;

import javax.annotation.Nonnull;

import freescape.AreaObject;

public enum FCLOpCode {
	CLRBIT(1, (state, inst) -> state.setFalse(inst.arg(0))), //
	SETBIT(1, (state, inst) -> state.setTrue(inst.arg(0))), //
	TOGBIT(1, (state, inst) -> state.toggleBoolean(inst.arg(0))), //
	DECVAR(1, (state, inst) -> state.decrement(inst.arg(0))), //
	INCVAR(1, (state, inst) -> state.increment(inst.arg(0))), //
	SETVAR(2, (state, inst) -> state.setVariable(inst.arg(0), inst.arg(1))), //

	ADDENERGY(1, (state, inst) -> state.changeStrength(inst.arg(0))), //
	ADDSTRGTH(1, (state, inst) -> state.changeStrength(inst.arg(0))), //
	ADDSCORE(3, (state, inst) -> state.increaseScore(inst.arg(0), inst.arg(1), inst.arg(2))), //

	DELAY(1, (state, inst) -> state.pause(inst.arg(0))), //
	MOVETO(2, (state, inst) -> state.moveToArea(inst.arg(0), inst.arg(1))), //
	NOP(0, DUMMY), //
	PRINT(1, (state, inst) -> state.showMessage(inst.arg(0))), //
	REDRAW(0, DUMMY), //
	SCREEN(1, (state, inst) -> state.showEffect(inst.arg(0))), //
	SOUND(1, (state, inst) -> state.playSfx(inst.arg(0))), //
	SPFX(1, DUMMY), //
	STARTANIM(1, (state, inst) -> state.triggerAnimation(inst.arg(0))), //
	SWAPJET(0, DUMMY), //
	SYNCSND(1, (state, inst) -> state.playSfxNextFrame(inst.arg(0))), //

	IF_BIT(2, true, (state, inst, scriptState) -> {
		if (!(state.getBoolean(inst.arg(0)) == inst.asBool(1)))
			scriptState.execute = false;
	}), //

	IF_INVIS(1, true, (state, inst, scriptState) -> {
		if (!state.getObject(inst.arg(0)).map(AreaObject::isInvisible).orElse(false))
			scriptState.execute = false;
	}), //
	IF_INVIS_A(2, true, (state, inst, scriptState) -> {
		if (!state.getObject(inst.arg(0), inst.arg(1)).map(AreaObject::isInvisible).orElse(false))
			scriptState.execute = false;
	}), //
	IF_VIS(1, true, (state, inst, scriptState) -> {
		if (!state.getObject(inst.arg(0)).map(AreaObject::isVisible).orElse(false))
			scriptState.execute = false;
	}), //
	IF_VIS_A(2, true, (state, inst, scriptState) -> {
		if (state.getObject(inst.arg(0), inst.arg(1)).map(AreaObject::isVisible).orElse(false))
			scriptState.execute = false;
	}), //
	IF_VAR(2, true, (state, inst, scriptState) -> {
		if (!((state.getVariable(inst.arg(0)) == inst.arg(1))))
			scriptState.execute = false;
	}), //
	IF_VAR_GT(2, true, (state, inst, scriptState) -> {
		if (!(state.getVariable(inst.arg(0)) > inst.arg(1)))
			scriptState.execute = false;
	}), //
	IF_VAR_LT(2, true, (state, inst, scriptState) -> {
		if (!(state.getVariable(inst.arg(0)) < inst.arg(1)))
			scriptState.execute = false;
	}), //
	ELSE(0, (state, inst, scriptState) -> {
		if (scriptState.ifsCount == scriptState.ifsTaken)
			scriptState.execute = !scriptState.execute;
	}), //
	ENDIF(0, (state, inst, scriptState) -> {
		if (scriptState.ifsCount == scriptState.ifsTaken) {
			scriptState.ifsTaken--;
			scriptState.execute = true;
		}
		scriptState.ifsCount--;
	}), //
	LOOP(1, (state, inst, scriptState) -> {
		scriptState.loopCount = inst.arg(0);
		scriptState.loopStart = scriptState.instIndex;
	}), //
	AGAIN(0, (state, inst, scriptState) -> {
		scriptState.loopCount--;
		if (scriptState.loopCount > 0)
			scriptState.instIndex = scriptState.loopStart;
	}), //

	DESTROY(1, (state, inst) -> state.getObject(inst.arg(0)).ifPresent(AreaObject::destroy)), //
	DESTROY_A(2, (state, inst) -> state.getObject(inst.arg(0), inst.arg(1)).ifPresent(AreaObject::destroy)), //
	INVIS(1, (state, inst) -> state.getObject(inst.arg(0)).ifPresent(AreaObject::hide)), //
	INVIS_A(2, (state, inst) -> state.getObject(inst.arg(0), inst.arg(1)).ifPresent(AreaObject::hide)), //
	TOGVIS(1, (state, inst) -> state.getObject(inst.arg(0)).ifPresent(AreaObject::toggleVisibility)), //
	TOGVIS_A(2, (state, inst) -> state.getObject(inst.arg(0), inst.arg(1)).ifPresent(AreaObject::toggleVisibility)), //
	VIS(1, (state, inst) -> state.getObject(inst.arg(0)).ifPresent(AreaObject::show)), //
	VIS_A(2, (state, inst) -> state.getObject(inst.arg(0), inst.arg(1)).ifPresent(AreaObject::show)), //

	EXECUTE(1, (state, inst, scriptState) -> {
		state.getObject(inst.arg(0))
			.map(AreaObject::getScript)
			.ifPresent(script -> script.exec(scriptState.trigger.orElse(null), state));
	}), //
	;

	private final int argumentCount;
	private final boolean conditional;
	private final TriConsumer<FCLGameState, FCLInstruction, FCLScriptState> opHandler;

	private FCLOpCode(int argumentCount, @Nonnull BiConsumer<FCLGameState, FCLInstruction> opHandler) {
		this(argumentCount, false, opHandler);
	}

	private FCLOpCode(int argumentCount, @Nonnull TriConsumer<FCLGameState, FCLInstruction, FCLScriptState> opHandler) {
		this(argumentCount, false, opHandler);
	}

	private FCLOpCode(int argumentCount, boolean conditional,
		@Nonnull BiConsumer<FCLGameState, FCLInstruction> opHandler) {

		this(argumentCount, conditional, (state, inst, scriptState) -> opHandler.accept(state, inst));
	}

	private FCLOpCode(int argumentCount, boolean conditional,
		@Nonnull TriConsumer<FCLGameState, FCLInstruction, FCLScriptState> opHandler) {

		this.argumentCount = argumentCount;
		this.conditional = conditional;
		this.opHandler = opHandler;
	}

	public int getArgumentCount() {
		return argumentCount;
	}

	public void exec(@Nonnull FCLGameState state, @Nonnull FCLInstruction inst, @Nonnull FCLScriptState scriptState) {
		final boolean doExecute = inst.shouldExecute(scriptState);
		System.out.printf("%s%s%n", inst, doExecute ? "" : " (SKIPPED)");
		if (doExecute) {
			opHandler.accept(state, inst, scriptState);
		}
		if (conditional) {
			scriptState.ifsCount++;
			if (doExecute)
				scriptState.ifsTaken++;
		}
	}

	private static interface TriConsumer<T, U, V> {
		void accept(T t, U u, V v);
	}
}
