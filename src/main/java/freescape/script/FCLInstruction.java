package freescape.script;

import static freescape.script.FCLOpCode.ELSE;
import static freescape.script.FCLOpCode.ENDIF;
import static freescape.script.FCLOpCode.EXECUTE;

import javax.annotation.Nonnull;

import io.vavr.collection.List;

public class FCLInstruction {
	private final int length;
	private final FCLTrigger trigger;
	private final FCLOpCode opcode;
	private final int[] arguments;

	public FCLInstruction(int length, @Nonnull FCLTrigger trigger, @Nonnull FCLOpCode opcode, int... arguments) {
		this.length = length;
		this.trigger = trigger;
		this.opcode = opcode;
		this.arguments = arguments;
	}

	public int arg(int index) {
		return arguments[index];
	}

	public boolean asBool(int index) {
		return arguments[index] > 0;
	}

	public int getLength() {
		return length;
	}

	@Nonnull
	public FCLTrigger getTrigger() {
		return trigger;
	}

	@Nonnull
	public FCLOpCode getOpcode() {
		return opcode;
	}

	public boolean matches(FCLTrigger t) {
		return t == null || t == getTrigger() || EXECUTE.equals(getOpcode());
	}

	public boolean shouldExecute(FCLScriptState scriptState) {
		boolean isConditionalChange = ENDIF.equals(getOpcode()) || ELSE.equals(getOpcode());
		return scriptState.execute || isConditionalChange;
	}

	@Override
	public String toString() {
		return String.format("[%11s]: %s(%s)", trigger, opcode, List.ofAll(arguments).mkCharSeq(", "));
	}
}
