package engine;

import java.util.Optional;

import javax.annotation.Nonnull;

import io.vavr.collection.HashSet;

import org.joml.Vector3i;
import org.joml.Vector3ic;

import data.DataContainer;
import freescape.Area;
import freescape.AreaObject;
import freescape.group.GroupHandler;
import freescape.script.FCLGameState;
import freescape.script.FCLTrigger;
import openxr.Stage;

public class FreescapeEngine implements AreaProvider, FCLGameState {
	private static final int TICK_MOVE = 50;
	private static final int TICK_TURN = 100;
	private static final int TICK_TIMER = 2000;
	private static final int TICK_SHOOT = 500;
	private static final int TICK_GROUP = 100;

	private final Player player;
	private final DataContainer data;

	private final boolean[] booleanValues;
	private final int[] variableValues;

	private Area currentArea;

	private long lastMoveTick, lastTurnTick, lastTimerTick, lastShootLTick, lastShootRTick, lastGroupTick;

	public FreescapeEngine(@Nonnull Stage stage, @Nonnull DataContainer data) {
		this.data = data;
		this.booleanValues = new boolean[255];
		this.variableValues = new int[255];
		this.player = new Player(stage, this, this);
		this.player.setMoveMode(data.defaultMovement());
		changeTo(data.getStartingArea(), data.getStartingEntrance());
	}

	@Override
	public Area getCurrentArea() {
		return currentArea;
	}

	@Override
	public Area getSharedArea() {
		return data.getArea(255);
	}

	public void nextArea() {
		final int nextAreaId = data.getAreaIds()
			.dropUntil(id -> id > getCurrentArea().getId())
			.headOption()
			.getOrElse(() -> data.getAreaIds().head());
		final int entranceId = data.getArea(nextAreaId).getEntranceIds().filter(i -> i != 255).head();
		changeTo(nextAreaId, entranceId);
	}

	private void changeTo(int areaId, int entranceId) {
		final Area area = data.getArea(areaId);
		if (area == null) {
			return;
		}
		final Area sharedArea = getSharedArea();

		AreaObject entrance = area.getEntrance(entranceId).orElse(null);
		if (entrance == null) {
			entrance = sharedArea.getEntrance(entranceId).orElse(null);
			if (entrance == null) {
				return;
			}
		}
		final Vector3ic pos = new Vector3i(entrance.getPos()).setComponent(0, -entrance.getPos().x());
		final Vector3ic size = entrance.getSize();

		System.out.printf("Going to area %d, entrance %d%n", areaId, entranceId);
		sharedArea.getObjects().peek(AreaObject::reset).forEach(AreaObject::setInvisible);
		currentArea = area;
		currentArea.getEntrance(255).map(AreaObject::getOrdinates).map(HashSet::ofAll).ifPresent(sharedIds -> {
			sharedIds.map(sharedArea::getObject)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.forEach(AreaObject::setVisible);
		});
		player.setAreaScale(area.getScale());
		player.moveTo(pos, size);
	}

	public void handleMovement(MovementAction action) {
		final long tick = System.currentTimeMillis();
		if (tick - lastMoveTick > TICK_MOVE) {
			lastMoveTick = tick;
			player.move(action);
		}
	}

	public void rotateLeft() {
		final long tick = System.currentTimeMillis();
		if (tick - lastTurnTick > TICK_TURN) {
			lastTurnTick = tick;
			player.rotateLeft();
		}
	}

	public void rotateRight() {
		final long tick = System.currentTimeMillis();
		if (tick - lastTurnTick > TICK_TURN) {
			lastTurnTick = tick;
			player.rotateRight();
		}
	}

	public void updateState() {
		this.data.getGlobalScripts().forEach(script -> script.exec(FCLTrigger.COLLISION, this));
		getCurrentArea().getScripts().forEach(script -> script.exec(FCLTrigger.COLLISION, this));

		final long tick = System.currentTimeMillis();
		if (tick - lastTimerTick > TICK_TIMER) {
			lastTimerTick = tick;
			this.data.getGlobalScripts().forEach(script -> script.exec(FCLTrigger.TIMER, this));
			getCurrentArea().getScripts().forEach(script -> script.exec(FCLTrigger.TIMER, this));
		}
		if (tick - lastGroupTick > TICK_GROUP) {
			lastGroupTick = tick;
			getSharedArea().getGroups().filter(GroupHandler::isRunnable).forEach(group -> group.update(this));
			getCurrentArea().getGroups().filter(GroupHandler::isRunnable).forEach(group -> group.update(this));
		}
		if (isCastle()) {
			if (getVariable(32) > 0) {
				setTrue(getVariable(32));
				setVariable(32, 0);
			}
		}
	}

	private boolean isCastle() {
		// CM1/2 TODO better way to identify
		return data.getAreasCount() == 104 || data.getAreasCount() == 49;
	}

	public void shootLeft() {
		final long tick = System.currentTimeMillis();
		if (tick - lastShootLTick > TICK_SHOOT) {
			lastShootLTick = tick;
			player.shootLeft();
		}
	}

	public void shootRight() {
		final long tick = System.currentTimeMillis();
		if (tick - lastShootRTick > TICK_SHOOT) {
			lastShootRTick = tick;
			player.shootRight();
		}
	}

	public void interactLeft() {
		player.interactLeft();
	}

	public void interactRight() {
		player.interactRight();
	}

	@Override
	public boolean getBoolean(int booleanId) {
		return booleanValues[booleanId - 1];
	}

	@Override
	public void setFalse(int booleanId) {
		booleanValues[booleanId - 1] = false;
	}

	@Override
	public void setTrue(int booleanId) {
		booleanValues[booleanId - 1] = true;
	}

	@Override
	public void toggleBoolean(int booleanId) {
		booleanValues[booleanId - 1] = !booleanValues[booleanId - 1];
	}

	@Override
	public int getVariable(int variableId) {
		return variableValues[variableId - 1];
	}

	@Override
	public void decrement(int variableId) {
		variableValues[variableId - 1]--;
	}

	@Override
	public void increment(int variableId) {
		variableValues[variableId - 1]++;
	}

	@Override
	public void setVariable(int variableId, int value) {
		variableValues[variableId - 1] = value;
	}

	@Override
	public void triggerAnimation(int objectId) {
		getSharedArea().getGroups()
			.appendAll(getCurrentArea().getGroups())
			.filter(g -> g.getControlledObjectId1() == objectId || g.getControlledObjectId2() == objectId
				|| g.getControlledObjectId3() == objectId)
			.forEach(g -> g.setCanContinue(true));
	}

	@Override
	public Optional<AreaObject> getObject(int objectId) {
		return getSharedArea().getObject(objectId).or(() -> getCurrentArea().getObject(objectId));
	}

	@Override
	public Optional<AreaObject> getObject(int areaId, int objectId) {
		return data.getArea(areaId).getObject(objectId);
	}

	@Override
	public void changeStrength(int strength) {
		// TODO Auto-generated method stub

	}

	@Override
	public void increaseScore(int score1, int score2, int score3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void moveToArea(int areaId, int entranceId) {
		changeTo(areaId, entranceId);
	}

	@Override
	public void pause(int length) {
		// TODO Auto-generated method stub

	}

	@Override
	public void playSfx(int sfxId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void playSfxNextFrame(int sfxId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void showEffect(int code) {
		// TODO Auto-generated method stub

	}

	@Override
	public void showMessage(int msgId) {
		// TODO Auto-generated method stub

	}
}
