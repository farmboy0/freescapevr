package engine;

import java.util.function.ObjIntConsumer;

import javax.annotation.Nonnull;

import org.joml.Matrix4fc;
import org.joml.Vector3f;

public enum MovementAction {
	MOVE_FOWARD((step, stepWidth) -> step.set(0, 0, -stepWidth)), //
	MOVE_BACKWARD((step, stepWidth) -> step.set(0, 0, stepWidth)), //
	STEP_LEFT((step, stepWidth) -> step.set(-stepWidth, 0, 0)), //
	STEP_RIGHT((step, stepWidth) -> step.set(stepWidth, 0, 0)), //
	;

	private final Vector3f step = new Vector3f();
	private final ObjIntConsumer<Vector3f> initialStepSetter;

	private MovementAction(@Nonnull ObjIntConsumer<Vector3f> initialStepSetter) {
		this.initialStepSetter = initialStepSetter;
	}

	public void computePosition(@Nonnull Matrix4fc player2World, @Nonnull Vector3f position, int stepWidth) {
		initialStepSetter.accept(step, stepWidth);
		player2World.transformPosition(step, position);
	}
}
