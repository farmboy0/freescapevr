package engine;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.Comparator;
import java.util.Objects;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;

import org.joml.Matrix4f;
import org.joml.Matrix4fc;
import org.joml.Quaternionfc;
import org.joml.RayAabIntersection;
import org.joml.Vector3f;
import org.joml.Vector3fc;
import org.joml.Vector3i;
import org.joml.Vector3ic;

import freescape.Area;
import freescape.AreaObject;
import freescape.MovementMode;
import freescape.script.FCLGameState;
import freescape.script.FCLScript;
import freescape.script.FCLTrigger;
import math.AABB;
import math.AABBc;
import math.Rectangle;
import openxr.Stage;

public class Player {
	private static final Vector3fc INTERACT_POSE_MIN = new Vector3f(0f);
	private static final Vector3fc INTERACT_POSE_MAX = new Vector3f(0.005f, 0.005f, -1f);

	private final Vector3f oldMin = new Vector3f();
	private final Vector3f oldMax = new Vector3f();
	private final Vector3f min = new Vector3f();
	private final Vector3f max = new Vector3f();

	private final Seq<Vector3fc> posBoundsVectors = API.Seq(min, max);
	private final Seq<Vector3fc> boundsVectors = API.Seq(oldMin, oldMax, min, max);

	private final Vector3f oldPos = new Vector3f();
	private final Vector3f pos = new Vector3f();

	private final Vector3i size = new Vector3i();

	private final AABB oldPosBounds = new AABB();
	private final AABB posBounds = new AABB();
	private final AABB moveBounds = new AABB();
	private final AABB fallBounds = new AABB();

	private final Stage stage;
	private final AreaProvider areaProvider;
	private final FCLGameState gameState;

	private int areaScale, stepWidth, maxClimb, maxFall;
	private MovementMode moveMode;

	public Player(@Nonnull Stage stage, @Nonnull AreaProvider areaProvider, @Nonnull FCLGameState gameState) {
		this.stage = stage;
		this.areaProvider = areaProvider;
		this.gameState = gameState;
		computePositionBounds();
	}

	public void move(@Nonnull MovementAction move) {
		computeMovementBounds(move);

		// Climb climbable collisions that were touched before movement
		getCollisions(moveBounds).filter(obj -> obj.intersects(oldPosBounds))
			.filter(obj -> (obj.upperYBound() - oldPos.y()) <= maxClimb)
			.maxBy(Comparator.comparing(AreaObject::upperYBound))
			.toJavaOptional()
			.ifPresent(obj -> {
				pos.set(oldPos).setComponent(1, obj.upperYBound());
				computePositionBounds();
				if (!getCollisions(posBounds).isEmpty()) {
					pos.set(oldPos);
					computePositionBounds();
				} else {
					stage.setPlayerWorldPosition(pos);
					computeMovementBounds(move);
				}
			});

		Rectangle walkableArea = moveBounds.xzRect();
		for (AreaObject obj : getCollisions(moveBounds)) {
			final Rectangle objBounds = obj.getBounds().xzRect();
			final Seq<Rectangle> walkableRegions = walkableArea.nonOverlappingRegions(objBounds)
				.filter(rect -> rect.contains(oldPos.x(), oldPos.z()))
				.filter(rect -> rect.xLength() >= size.x() && rect.yLength() >= size.z());
			if (walkableRegions.size() == 1) {
				walkableArea = walkableRegions.get(0);
			} else if (walkableRegions.size() == 2) {
				walkableArea = walkableRegions.get(0).overlapWith(walkableRegions.get(1)).orElseThrow();
			} else {
				System.err.println(String.format(
					"No walkable region, Area=%d, start=<%5.2f,%5.2f>, moveBounds=%s, walkable=%s, collision=%d",
					areaProvider.getCurrentArea().getId(), oldPos.x(), oldPos.z(), moveBounds, walkableArea,
					obj.getId()));
				walkableArea = null;
				break;
			}
		}

		if (walkableArea != null) {
			final float xMin = walkableArea.x1() + (0.5f * size.x());
			final float xMax = walkableArea.x2() - (0.5f * size.x());
			final float zMin = walkableArea.y1() + (0.5f * size.z());
			final float zMax = walkableArea.y2() - (0.5f * size.z());
			pos.set(max(min(pos.x(), xMax), xMin), pos.y(), max(min(pos.z(), zMax), zMin));
			computePositionBounds();
		}

		computeFallBounds();
		final int newY = getCollisions(fallBounds).maxBy(Comparator.comparing(AreaObject::upperYBound))
			.toJavaOptional()
			.map(AreaObject::upperYBound)
			.orElse(0);
		pos.setComponent(1, newY);
		computePositionBounds();

		stage.setPlayerWorldPosition(pos);
		getIntersections(posBounds).map(AreaObject::getScript)
			.filter(Objects::nonNull)
			.forEach(script -> script.exec(FCLTrigger.COLLISION, gameState));
	}

	private Set<AreaObject> getCollisions(AABBc bounds) {
		final Area area = areaProvider.getCurrentArea();
		final Area sharedArea = areaProvider.getSharedArea();
		return area.getCollisions(bounds).addAll(sharedArea.getCollisions(bounds));
	}

	private Set<AreaObject> getIntersections(AABBc bounds) {
		final Area area = areaProvider.getCurrentArea();
		final Area sharedArea = areaProvider.getSharedArea();
		return area.getIntersections(bounds).addAll(sharedArea.getIntersections(bounds));
	}

	private final Vector3f halfStepToFront = new Vector3f();

	public void moveTo(@Nonnull Vector3ic position, @Nonnull Vector3ic heading) {
		stage.setPlayerWorldPosition(pos.set(position));
		stage.rotate(180f - heading.y());
		computePositionBounds();

		while (!getCollisions(posBounds).isEmpty()) {
			stage.getPlayerToWorldMatrix().transformPosition(halfStepToFront.set(0f, 0f, -0.5f * size.x()));
			stage.setPlayerWorldPosition(pos.set(halfStepToFront));
			computePositionBounds();
		}
	}

	public void rotateLeft() {
		stage.rotateLeft();
	}

	public void rotateRight() {
		stage.rotateRight();
	}

	public void shootLeft() {
		shoot(stage.getLeftAimPosition(), stage.getLeftAimRotation());
	}

	public void shootRight() {
		shoot(stage.getRightAimPosition(), stage.getRightAimRotation());
	}

	private final Vector3f aimWorldOrigin = new Vector3f();
	private final Vector3f aimWorldDirection = new Vector3f();
	private final Matrix4f aimWorldMatrix = new Matrix4f();

	private void shoot(Vector3fc aimPosition, Quaternionfc aimRotation) {
		stage.getStageToWorldMatrix().transformPosition(aimPosition, aimWorldOrigin);
		stage.getStageToWorldMatrix()
			.rotate(aimRotation, aimWorldMatrix)
			.transformDirection(aimWorldDirection.set(0f, 0f, -1f));
		rayIntersection().forEach(this::shoot);
	}

	private Set<AreaObject> rayIntersection() {
		final RayAabIntersection intersection = new RayAabIntersection( //
			aimWorldOrigin.x, aimWorldOrigin.y, aimWorldOrigin.z, //
			aimWorldDirection.x, aimWorldDirection.y, aimWorldDirection.z //
		);
		return areaProvider.getCurrentArea()
			.getIntersections(intersection)
			.addAll(areaProvider.getSharedArea().getIntersections(intersection));
	}

	private void shoot(AreaObject obj) {
		final FCLScript script = obj.getScript();
		if (script != null) {
			script.exec(FCLTrigger.SHOOTING, gameState);
		}
	}

	public void interactLeft() {
		interact(stage.getLeftAimMatrix());
	}

	public void interactRight() {
		interact(stage.getRightAimMatrix());
	}

	private final Vector3f gripWorldMin = new Vector3f();
	private final Vector3f gripWorldMax = new Vector3f();
	private final Matrix4f grip2WorldMatrix = new Matrix4f();

	private void interact(Matrix4fc grip2StageMatrix) {
		stage.getStageToWorldMatrix().mulAffine(grip2StageMatrix, grip2WorldMatrix);
		grip2WorldMatrix.transformAab(INTERACT_POSE_MIN, INTERACT_POSE_MAX, gripWorldMin, gripWorldMax);
		getIntersections(gripWorldMin, gripWorldMax).forEach(obj -> interact(obj));
	}

	private Set<AreaObject> getIntersections(Vector3fc min, Vector3fc max) {
		return areaProvider.getCurrentArea()
			.getIntersections(min, max)
			.addAll(areaProvider.getSharedArea().getIntersections(min, max));
	}

	private void interact(AreaObject obj) {
		final FCLScript script = obj.getScript();
		if (script != null) {
			System.out.println("------------------------");
			System.out.printf("%15s(%3d,%3d):%n", obj.getType(), areaProvider.getCurrentArea().getId(), obj.getId());
			script.exec(FCLTrigger.INTERACTION, gameState);
		}
	}

	public void setAreaScale(int areaScale) {
		this.areaScale = areaScale;
		this.maxClimb = areaScale * 32;
		this.maxFall = areaScale * 32 * 2;
		updatePlayer();
	}

	public void setMoveMode(@Nonnull MovementMode moveMode) {
		this.moveMode = moveMode;
		updatePlayer();
	}

	private void updatePlayer() {
		final int playerHeight = areaScale * moveMode.getPlayerHeight();
		final int playerSize = areaScale * 8;
		this.size.set(playerSize, playerHeight, playerSize);
		this.stage.setPlayerWorldHeight(playerHeight);
		this.stepWidth = areaScale * moveMode.getStepWidth();

		computePositionBounds();
	}

	private void computePositionBounds() {
		this.min.set(pos).add(-size.x() / 2, 0, -size.z() / 2);
		this.max.set(pos).add(size.x() / 2, size.y(), size.z() / 2);
		this.posBounds.set(posBoundsVectors);
	}

	private void computeMovementBounds(@Nonnull MovementAction move) {
		oldPos.set(pos);
		oldPosBounds.set(posBounds);
		oldMin.set(min);
		oldMax.set(max);

		move.computePosition(stage.getPlayerToWorldMatrix(), pos, stepWidth);
		computePositionBounds();
		moveBounds.set(boundsVectors);
	}

	private void computeFallBounds() {
		oldMin.set(min.x(), 0, min.z());
		oldMax.set(max.x(), 0, max.z());
		this.fallBounds.set(boundsVectors);
	}
}
