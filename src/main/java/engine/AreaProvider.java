package engine;

import freescape.Area;

public interface AreaProvider {
	Area getCurrentArea();

	Area getSharedArea();
}
