package ui;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.joml.Matrix4f;
import org.joml.Matrix4fc;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11C;
import org.lwjgl.opengl.GL12C;
import org.lwjgl.opengl.GL15C;
import org.lwjgl.opengl.GL20C;
import org.lwjgl.opengl.GL30C;
import org.lwjgl.opengl.GL31C;
import org.lwjgl.opengl.GL43C;
import org.lwjgl.opengl.GL45C;
import org.lwjgl.system.MemoryUtil;

import opengl.ShaderProgram;
import openxr.Stage;

public class VirtualScreen {
	private static final float[] SCREEN_RECT_VERTS = new float[] { //
		// X, Y, Z, UV1, UV2
		-1.0f, -0.75f, 0f, 0.0f, 1.0f, //
		1.0f, -0.75f, 0f, 1.0f, 1.0f, //
		-1.0f, 0.75f, 0f, 0.0f, 0.0f, //
		-1.0f, 0.75f, 0f, 0.0f, 0.0f, //
		1.0f, -0.75f, 0f, 1.0f, 1.0f, //
		1.0f, 0.75f, 0f, 1.0f, 0.0f //
	};

	private final Vector3f screenPosition = new Vector3f();
	private final Matrix4f screen2Stage = new Matrix4f();
	private final Matrix4f screen2Proj = new Matrix4f();

	private final float[] matrixBuffer = new float[16];

	private final Stage stage;
	private final int textureId, screenRectVerts, screenVAO, screenUBO;
	private final ShaderProgram screenProgram;

	private Optional<BufferedImage> content = Optional.empty();

	public VirtualScreen(Stage stage) throws IOException {
		this.stage = stage;
		this.textureId = GL11C.glGenTextures();

		screenRectVerts = GL15C.glGenBuffers();
		GL15C.glBindBuffer(GL15C.GL_ARRAY_BUFFER, screenRectVerts);
		GL15C.glBufferData(GL15C.GL_ARRAY_BUFFER, SCREEN_RECT_VERTS, GL15C.GL_STATIC_DRAW);

		screenVAO = GL30C.glGenVertexArrays();
		GL30C.glBindVertexArray(screenVAO);
		// Position vertex attribute
		GL45C.glVertexArrayAttribBinding(screenVAO, 0, 0);
		GL45C.glVertexArrayAttribFormat(screenVAO, 0, 3, GL11C.GL_FLOAT, false, 0);
		GL45C.glEnableVertexArrayAttrib(screenVAO, 0);
		// TexCoord vertex attribute
		GL45C.glVertexArrayAttribBinding(screenVAO, 1, 0);
		GL45C.glVertexArrayAttribFormat(screenVAO, 1, 2, GL11C.GL_FLOAT, false, 3 * 4);
		GL45C.glEnableVertexArrayAttrib(screenVAO, 1);
		GL30C.glBindVertexArray(0);

		screenProgram = new ShaderProgram("screen");

		screenUBO = GL15C.glGenBuffers();
	}

	public Optional<BufferedImage> getContent() {
		return content;
	}

	public void changeContent(@Nullable BufferedImage image) {
		changeContent(Optional.ofNullable(image));
	}

	public void changeContent(@Nonnull Optional<BufferedImage> content) {
		this.content = content;
		content.ifPresent(this::createTexture);
		screenPosition.set(stage.getPlayerStagePosition().x(), stage.getPlayerStageHeight(), -10);
		screen2Stage.translationRotateScale(screenPosition, stage.getPlayerStageOrientation(), 5);
	}

	public void render(Matrix4fc viewProjection, Matrix4fc stage2View) {
		getContent().ifPresent(image -> {
			// screen -> stage -> view -> projection
			final float[] ubo = viewProjection.mulAffineR(stage2View, screen2Proj)
				.mulAffineR(screen2Stage, screen2Proj)
				.get(matrixBuffer);
			GL15C.glBindBuffer(GL31C.GL_UNIFORM_BUFFER, screenUBO);
			GL15C.glBufferData(GL31C.GL_UNIFORM_BUFFER, ubo, GL15C.GL_STREAM_DRAW);
			GL15C.glBindBuffer(GL31C.GL_UNIFORM_BUFFER, 0);
			GL30C.glBindBufferBase(GL31C.GL_UNIFORM_BUFFER, 1, screenUBO);

			GL30C.glBindVertexArray(screenVAO);
			GL43C.glBindVertexBuffer(0, screenRectVerts, 0, 5 * 4);
			screenProgram.useProgram();
			GL11C.glBindTexture(GL11C.GL_TEXTURE_2D, textureId);
			GL11C.glDrawArrays(GL11C.GL_TRIANGLES, 0, 6);
			GL11C.glBindTexture(GL11C.GL_TEXTURE_2D, 0);
			GL20C.glUseProgram(0);
			GL30C.glBindVertexArray(0);
		});
	}

	private void createTexture(BufferedImage image) {
		final IntBuffer imageData = MemoryUtil.memAllocInt(image.getWidth() * image.getHeight());
		imageData.put(image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth())).rewind();
		GL11C.glBindTexture(GL11C.GL_TEXTURE_2D, textureId);
		GL11C.glTexImage2D(GL11C.GL_TEXTURE_2D, 0, GL11C.GL_RGB8, 320, 200, 0, GL12C.GL_BGRA, GL11C.GL_UNSIGNED_BYTE,
			imageData);
		GL11C.glTexParameteri(GL11C.GL_TEXTURE_2D, GL11C.GL_TEXTURE_WRAP_S, GL11C.GL_REPEAT);
		GL11C.glTexParameteri(GL11C.GL_TEXTURE_2D, GL11C.GL_TEXTURE_WRAP_T, GL11C.GL_REPEAT);
		GL11C.glTexParameteri(GL11C.GL_TEXTURE_2D, GL11C.GL_TEXTURE_MAG_FILTER, GL11C.GL_LINEAR);
		GL11C.glTexParameteri(GL11C.GL_TEXTURE_2D, GL11C.GL_TEXTURE_MIN_FILTER, GL11C.GL_LINEAR);
		GL11C.glBindTexture(GL11C.GL_TEXTURE_2D, 0);
	}
}
