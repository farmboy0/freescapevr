package ui;

import java.io.IOException;
import java.nio.ByteBuffer;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Map;

import org.joml.Matrix4f;
import org.joml.Matrix4fc;
import org.joml.Quaternionfc;
import org.joml.Vector3fc;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL11C;
import org.lwjgl.opengl.GL12C;
import org.lwjgl.opengl.GL14C;
import org.lwjgl.opengl.GL15C;
import org.lwjgl.opengl.GL20C;
import org.lwjgl.opengl.GL30C;
import org.lwjgl.opengl.GL31C;
import org.lwjgl.opengl.GL43C;
import org.lwjgl.opengl.GL45C;

import engine.AreaProvider;
import freescape.render.AreaRenderer;
import opengl.ShaderProgram;
import openxr.OpenXRView;
import openxr.OpenXRView.ProjectionView;
import openxr.Stage;

public class FreescapeVRRenderer {
	private static final float[] WINDOW_RECT_VERTS = new float[] { //
		// X, Y, UV1, UV2
		-1.0f, -1.0f, 0.0f, 0.0f, //
		1.0f, -1.0f, 1.0f, 0.0f, //
		-1.0f, 1.0f, 0.0f, 1.0f, //
		-1.0f, 1.0f, 0.0f, 1.0f, //
		1.0f, -1.0f, 1.0f, 0.0f, //
		1.0f, 1.0f, 1.0f, 1.0f //
	};

	private static final int[] CUBE_INDEXES = { //
		4, 0, 6, 6, 0, 2, // left
		1, 5, 3, 3, 5, 7, // right
		1, 0, 5, 5, 0, 4, // bottom
		2, 3, 6, 6, 3, 7, // top
		0, 1, 2, 2, 1, 3, // front
		5, 4, 7, 7, 4, 6, // back
	};

	private static final float P_LEFT = 0.00f, P_BTM = 0.00f, P_FRNT = 0.0f;
	private static final float P_RGHT = 0.005f, P_TOP = 0.005f, P_BACK = -1.0f;
	private static final float[][] P_POINTS = new float[][] { //
		{ P_LEFT, P_BTM, P_FRNT }, { P_RGHT, P_BTM, P_FRNT }, { P_LEFT, P_TOP, P_FRNT }, { P_RGHT, P_TOP, P_FRNT }, //
		{ P_LEFT, P_BTM, P_BACK }, { P_RGHT, P_BTM, P_BACK }, { P_LEFT, P_TOP, P_BACK }, { P_RGHT, P_TOP, P_BACK } //
	};
	private static final float[] P_COLOR = new float[] { 0.34f, 1.0f, 1.0f, 1.0f };

	private static final float[] POINTER_VERTS = new float[6 * 6 * (3 + 4)];
	static {
		for (int f = 0; f < 6; f++) {
			for (int i = 0; i < 6; i++) {
				System.arraycopy(P_POINTS[CUBE_INDEXES[f * 6 + i]], 0, POINTER_VERTS, (f * 6 * 7) + (i * 7), 3);
				System.arraycopy(P_COLOR, 0, POINTER_VERTS, (f * 6 * 7) + (i * 7) + 3, 4);
			}
		}
	}

	private static final float H_LEFT = -0.05f, H_BTM = -0.05f, H_FRNT = 0.05f;
	private static final float H_RGHT = 0.05f, H_TOP = 0.05f, H_BACK = -0.05f;
	private static final float[][] H_POINTS = new float[][] { //
		{ H_LEFT, H_BTM, H_FRNT }, { H_RGHT, H_BTM, H_FRNT }, { H_LEFT, H_TOP, H_FRNT }, { H_RGHT, H_TOP, H_FRNT }, //
		{ H_LEFT, H_BTM, H_BACK }, { H_RGHT, H_BTM, H_BACK }, { H_LEFT, H_TOP, H_BACK }, { H_RGHT, H_TOP, H_BACK } //
	};
	private static final float[] H_COLOR1 = new float[] { 1.0f, 0.0f, 0.0f, 1.0f };
	private static final float[] H_COLOR2 = new float[] { 0.0f, 1.0f, 0.0f, 1.0f };
	private static final float[] H_COLOR3 = new float[] { 0.0f, 0.0f, 1.0f, 1.0f };

	private static final float[] HAND_VERTS = new float[6 * 6 * (3 + 4)];
	static {
		for (int f = 0; f < 6; f++) {
			final float[] color = f < 2 ? H_COLOR1 : f < 4 ? H_COLOR2 : H_COLOR3;
			for (int i = 0; i < 6; i++) {
				System.arraycopy(H_POINTS[CUBE_INDEXES[f * 6 + i]], 0, HAND_VERTS, (f * 6 * 7) + (i * 7), 3);
				System.arraycopy(color, 0, HAND_VERTS, (f * 6 * 7) + (i * 7) + 3, 4);
			}
		}
	}

	private static final AreaRenderer RENDERER = new AreaRenderer();

	private final Matrix4f viewProjectionMatrix = new Matrix4f();
	private final Matrix4f modelviewMatrix = new Matrix4f();
	private final Matrix4f stage2View = new Matrix4f();

	private final Matrix4f handMatrix = new Matrix4f();

	private final float[] matrixBuffer = new float[16];

	private final Stage stage;
	private final VirtualScreen screen;
	private final int framebuffer;
	private final Map<Integer, Integer> depthTextures;

	private int windowRectVerts, pointerVerts, handVerts;
	private int colorVAO, colorUBO, textureVAO;
	private ShaderProgram colorProgram, textureProgram;

	public FreescapeVRRenderer(@Nonnull Stage stage, @Nonnull OpenXRView[] views) throws IOException {
		this.stage = stage;
		this.screen = new VirtualScreen(stage);
		this.framebuffer = GL30C.glGenFramebuffers();
		this.depthTextures = API.Seq(views).flatMap(view -> {
			return Array.range(0, view.getImageCount()).toMap(view::getTexture, i -> {
				int texture = GL11C.glGenTextures();
				GL11C.glBindTexture(GL11C.GL_TEXTURE_2D, texture);
				GL11C.glTexParameteri(GL11C.GL_TEXTURE_2D, GL11C.GL_TEXTURE_MAG_FILTER, GL11C.GL_NEAREST);
				GL11C.glTexParameteri(GL11C.GL_TEXTURE_2D, GL11C.GL_TEXTURE_MIN_FILTER, GL11C.GL_NEAREST);
				GL11C.glTexParameteri(GL11C.GL_TEXTURE_2D, GL11C.GL_TEXTURE_WRAP_S, GL12C.GL_CLAMP_TO_EDGE);
				GL11C.glTexParameteri(GL11C.GL_TEXTURE_2D, GL11C.GL_TEXTURE_WRAP_T, GL12C.GL_CLAMP_TO_EDGE);
				GL11C.glTexImage2D(GL11C.GL_TEXTURE_2D, 0, GL14C.GL_DEPTH_COMPONENT32, view.getWidth(),
					view.getHeight(), 0, GL11C.GL_DEPTH_COMPONENT, GL11C.GL_FLOAT, (ByteBuffer) null);
				return texture;
			});
		}).toMap(Tuple2::_1, Tuple2::_2);
		GL11C.glBindTexture(GL11C.GL_TEXTURE_2D, 0);

		initGL();
	}

	private void initGL() throws IOException {
		GL11C.glDisable(GL11.GL_LIGHTING);
		GL11C.glEnable(GL11C.GL_DEPTH_TEST);

		windowRectVerts = GL15C.glGenBuffers();
		GL15C.glBindBuffer(GL15C.GL_ARRAY_BUFFER, windowRectVerts);
		GL15C.glBufferData(GL15C.GL_ARRAY_BUFFER, WINDOW_RECT_VERTS, GL15C.GL_STATIC_DRAW);

		pointerVerts = GL15C.glGenBuffers();
		GL15C.glBindBuffer(GL15C.GL_ARRAY_BUFFER, pointerVerts);
		GL15C.glBufferData(GL15C.GL_ARRAY_BUFFER, POINTER_VERTS, GL15C.GL_STATIC_DRAW);

		handVerts = GL15C.glGenBuffers();
		GL15C.glBindBuffer(GL15C.GL_ARRAY_BUFFER, handVerts);
		GL15C.glBufferData(GL15C.GL_ARRAY_BUFFER, HAND_VERTS, GL15C.GL_STATIC_DRAW);

		GL15C.glBindBuffer(GL15C.GL_ARRAY_BUFFER, 0);

		colorVAO = GL30C.glGenVertexArrays();
		GL30C.glBindVertexArray(colorVAO);
		// Position vertex attribute
		GL45C.glVertexArrayAttribBinding(colorVAO, 0, 0);
		GL45C.glVertexArrayAttribFormat(colorVAO, 0, 3, GL11C.GL_FLOAT, false, 0);
		GL45C.glEnableVertexArrayAttrib(colorVAO, 0);
		// Color vertex attribute
		GL45C.glVertexArrayAttribBinding(colorVAO, 1, 0);
		GL45C.glVertexArrayAttribFormat(colorVAO, 1, 4, GL11C.GL_FLOAT, false, 3 * 4);
		GL45C.glEnableVertexArrayAttrib(colorVAO, 1);

		textureVAO = GL30C.glGenVertexArrays();
		GL30C.glBindVertexArray(textureVAO);
		// Position vertex attribute
		GL45C.glVertexArrayAttribBinding(textureVAO, 0, 0);
		GL45C.glVertexArrayAttribFormat(textureVAO, 0, 2, GL11C.GL_FLOAT, false, 0);
		GL45C.glEnableVertexArrayAttrib(textureVAO, 0);
		// TexCoord vertex attribute
		GL45C.glVertexArrayAttribBinding(textureVAO, 1, 0);
		GL45C.glVertexArrayAttribFormat(textureVAO, 1, 2, GL11C.GL_FLOAT, false, 2 * 4);
		GL45C.glEnableVertexArrayAttrib(textureVAO, 1);

		GL30C.glBindVertexArray(0);

		colorProgram = new ShaderProgram("color");
		textureProgram = new ShaderProgram("texture");

		colorUBO = GL15C.glGenBuffers();
	}

	public VirtualScreen getScreen() {
		return screen;
	}

	public void renderScreen(OpenXRView view, boolean renderLeftHand, boolean renderRightHand) {
		renderPre(view);
		screen.render(viewProjectionMatrix, stage2View);
		renderPost(renderLeftHand, renderRightHand);
	}

	public void renderWorld(AreaProvider areaProvider, OpenXRView view, boolean renderLeftHand,
		boolean renderRightHand) {

		renderPre(view);
		RENDERER.render(areaProvider);
		renderPost(renderLeftHand, renderRightHand);
	}

	private void renderPre(OpenXRView view) {
		GL30C.glBindFramebuffer(GL30C.GL_FRAMEBUFFER, framebuffer);

		GL30C.glFramebufferTexture2D(GL30C.GL_FRAMEBUFFER, GL30C.GL_COLOR_ATTACHMENT0, GL11C.GL_TEXTURE_2D,
			view.getCurrentTexture(), 0);
		GL30C.glFramebufferTexture2D(GL30C.GL_FRAMEBUFFER, GL30C.GL_DEPTH_ATTACHMENT, GL11C.GL_TEXTURE_2D,
			depthTextures.get(view.getCurrentTexture()).get(), 0);

		final ProjectionView projView = view.getProjectionView();
		updateScreenProjection(projView);
		updateModelView(projView);

		GL11C.glViewport(projView.getSubImageOffsetX(), projView.getSubImageOffsetY(), projView.getSubImageWidth(),
			projView.getSubImageHeight());
		GL11.glClearColor(0f, 0f, 0f, 1.0f);
		GL11.glClear(GL11C.GL_COLOR_BUFFER_BIT | GL11C.GL_DEPTH_BUFFER_BIT | GL11C.GL_STENCIL_BUFFER_BIT);
		GL11.glClearDepth(1.0f);
	}

	private void renderPost(boolean renderLeftHand, boolean renderRightHand) {
		renderHands(renderLeftHand, renderRightHand);
		GL30C.glBindFramebuffer(GL30C.GL_FRAMEBUFFER, 0);
	}

	private void renderHands(boolean renderLeftHand, boolean renderRightHand) {
		if (renderLeftHand) {
			renderPose(stage.getLeftAimMatrix(), pointerVerts);
			renderPose(stage.getLeftGripMatrix(), handVerts);
		}
		if (renderRightHand) {
			renderPose(stage.getRightAimMatrix(), pointerVerts);
			renderPose(stage.getRightGripMatrix(), handVerts);
		}
	}

	private void renderPose(Matrix4fc pose2Stage, int poseModelBuffer) {
		// pose -> stage -> view -> projection
		final float[] ubo = viewProjectionMatrix.mulAffineR(stage2View, handMatrix)
			.mulAffineR(pose2Stage, handMatrix)
			.get(matrixBuffer);
		GL15C.glBindBuffer(GL31C.GL_UNIFORM_BUFFER, colorUBO);
		GL15C.glBufferData(GL31C.GL_UNIFORM_BUFFER, ubo, GL15C.GL_STREAM_DRAW);
		GL15C.glBindBuffer(GL31C.GL_UNIFORM_BUFFER, 0);

		GL30C.glBindVertexArray(colorVAO);
		GL43C.glBindVertexBuffer(0, poseModelBuffer, 0, 7 * 4);
		colorProgram.useProgram();
		GL30C.glBindBufferBase(GL31C.GL_UNIFORM_BUFFER, 1, colorUBO);
		GL11C.glDrawArrays(GL11C.GL_TRIANGLES, 0, 6 * 6);
		GL20C.glUseProgram(0);
		GL30C.glBindVertexArray(0);
	}

	public void renderToWindow(OpenXRView view, int viewIndex, int ww, int wh) {
		int wh2 = (int) (((float) view.getHeight() / view.getWidth()) * ww);
		if (wh2 > wh) {
			int ww2 = (int) (((float) view.getWidth() / view.getHeight()) * wh);
			GL11C.glViewport(ww2 * viewIndex, 0, ww2, wh);
		} else {
			GL11C.glViewport(ww * viewIndex, 0, ww, wh2);
		}

		GL30C.glBindVertexArray(textureVAO);
		GL43C.glBindVertexBuffer(0, windowRectVerts, 0, 4 * 4);
		textureProgram.useProgram();
		GL11C.glBindTexture(GL11C.GL_TEXTURE_2D, view.getCurrentTexture());
		GL11C.glDisable(GL11C.GL_DEPTH_TEST);
		GL11C.glDrawArrays(GL11C.GL_TRIANGLES, 0, 6);
		GL11C.glEnable(GL11C.GL_DEPTH_TEST);
		GL11C.glBindTexture(GL11C.GL_TEXTURE_2D, 0);
		GL20C.glUseProgram(0);
		GL30C.glBindVertexArray(0);
	}

	private void updateScreenProjection(ProjectionView projView) {
		final float left = projView.getFoVAngleLeft();
		final float right = projView.getFoVAngleRight();
		final float down = projView.getFoVAngleDown();
		final float up = projView.getFoVAngleUp();

		viewProjectionMatrix.setPerspectiveOffCenterFov(left, right, down, up, 0.05f, 8192.0f);

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadMatrixf(viewProjectionMatrix.get(matrixBuffer));
	}

	private void updateModelView(ProjectionView projView) {
		final Vector3fc viewPos = projView.getPosition();
		final Quaternionfc viewOrient = projView.getOrientation();

		// model/world -> stage -> view
		stage2View.translationRotateInvert(viewPos, viewOrient);
		stage2View.mulAffine(stage.getWorldToStageMatrix(), modelviewMatrix);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadMatrixf(modelviewMatrix.get(matrixBuffer));
	}

	public void flush() {
		GL11C.glFlush();
	}

	public void finish() {
		GL11C.glFinish();

		for (int texture : depthTextures.values()) {
			GL11C.glDeleteTextures(texture);
		}
	}
}