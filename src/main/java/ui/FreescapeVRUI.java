package ui;

import static io.vavr.API.Seq;
import static org.lwjgl.system.APIUtil.apiUnknownToken;

import java.io.IOException;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;

import org.joml.Quaternionfc;
import org.joml.Vector2fc;
import org.joml.Vector3f;
import org.joml.Vector3fc;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL43C;
import org.lwjgl.opengl.GLDebugMessageCallback;

import data.DataContainer;
import engine.FreescapeEngine;
import engine.MovementAction;
import freescape.ImageType;
import glfw.GLFWException;
import glfw.GLFWRuntime;
import glfw.GLFWWindow;
import glfw.GLFWWindowHintsBuilder;
import openxr.OpenXR1_0;
import openxr.OpenXRAPILayer;
import openxr.OpenXRActionSet;
import openxr.OpenXRActionSet.BooleanAction.BooleanState;
import openxr.OpenXRActionSet.FloatAction.FloatState;
import openxr.OpenXRActionSet.PoseAction.PoseState;
import openxr.OpenXRActionSet.Vector2fAction.Vector2fState;
import openxr.OpenXRDebugListener;
import openxr.OpenXREventListener;
import openxr.OpenXRException;
import openxr.OpenXRExtension;
import openxr.OpenXRInstance;
import openxr.OpenXRInstance.OpenXRGLSupport;
import openxr.OpenXRInstance.ViewConfig;
import openxr.OpenXRSession;
import openxr.OpenXRSessionState;
import openxr.OpenXRView;
import openxr.OpenXRView.ProjectionView;
import openxr.Stage;
import openxr_glfw.OpenXRSessionGLFWConnector;

public class FreescapeVRUI {
	private static final Set<Integer> GL_FORMATS = API.Set( //
		GL11.GL_RGB10_A2, GL30.GL_RGBA16F, GL11.GL_RGBA8, GL31.GL_RGBA8_SNORM, GL21.GL_SRGB8_ALPHA8 //
	);

	private final GLFWWindow window;

	private final OpenXRInstance xrInstance;
	private final OpenXRSession xrSession;
	private final OpenXRView[] views;
	private final OpenXRActionSet xrActionSet;

	private Stage stage;
	private FreescapeEngine engine;
	private FreescapeVRRenderer renderer;
	private UiState currentState;

	public FreescapeVRUI() throws GLFWException, OpenXRException, IOException {
		final Seq<String> extensions = OpenXRExtension.builder()
			.require(OpenXRExtension.XR_KHR_OPENGL_ENABLE)
			.useIfAvailable(OpenXRExtension.XR_EXT_DEBUG_UTILS)
			.useIfAvailable(OpenXRExtension.XR_MNDX_EGL_ENABLE)
			.build();
		final boolean useEGL = OpenXRExtension.XR_MNDX_EGL_ENABLE.isAvailable();
		final Seq<String> apiLayers = useEGL ? Seq() : OpenXRAPILayer.builder()
			.useIfAvailable(OpenXRAPILayer.XR_APILAYER_LUNARG_CORE_VALIDATION)
			.build();

		System.out.println("Available OpenXR extensions:");
		OpenXRExtension.EXTENSIONS.forEach(System.out::println);
		System.out.println("Available OpenXR api layers:");
		OpenXRAPILayer.API_LAYERS.forEach(System.out::println);

		xrInstance = OpenXR1_0.createInstance("FreescapeVR", extensions, apiLayers);

		final OpenXRDebugListener cb = (severity, channels, messageId, functionName, message, labels) -> {
			System.out.printf("[XR][%s][%s] - %s%n", severity, functionName, message);
			return 0;
		};
		xrInstance.enableDebugging(cb);

		xrInstance.register(new OpenXREventListenerImpl());

		final OpenXRGLSupport glSupport = xrInstance.determineGLSupport();
		GLFWWindowHintsBuilder.INSTANCE.openGLMajorVersion(glSupport.majorMax())
			.openGLMinorVersion(glSupport.minorMax())
			.useCompatProfile()
			.doubleBuffered(false)
			.useEGL(useEGL)
			.build();
		window = new GLFWWindow(1008, 560, "FreescapeVR");
		window.makeCurrent();
		GL.createCapabilities();
		final GLDebugMessageCallback glCB = GLDebugMessageCallback
			.create((source, type, id, severity, length, message, userParam) -> {
				if (severity == GL43C.GL_DEBUG_SEVERITY_NOTIFICATION) {
					return;
				}
				System.out.printf("[GL][%s][%s][%s] - %s%n", toType(type), toSeverity(severity), toSource(source),
					GLDebugMessageCallback.getMessage(length, message));
			});
		GL43C.glDebugMessageCallback(glCB, 0);

		xrSession = xrInstance.createSessionFor(new OpenXRSessionGLFWConnector(window, useEGL));
		xrSession.createReferenceSpace();

		final Seq<Long> formats = xrSession.enumSwapchainFormats();
		final int format = GL_FORMATS.find(fmt -> formats.contains(fmt.longValue()))
			.getOrElseThrow(IllegalStateException::new);

		final Seq<ViewConfig> viewConfigs = xrInstance.enumViewConfigurationViews();
		views = xrSession.initViews(viewConfigs, format);

		xrActionSet = xrInstance.createActionSet("freescape", "Freescape")
			.addBooleanAction("left_accept", "Left Hand Accept")
			.onDaydreamUseLeftSelectClick()
			.onViveUseLeftTriggerClick()
			.onWMRUseLeftTriggerValue()
			.onOculusGoUseLeftTriggerClick()
			.onOculusTouchUseLeftTriggerValue()
			.onIndexUseLeftTriggerClick()
			.addBooleanAction("right_accept", "Right Hand Accept")
			.onDaydreamUseRightSelectClick()
			.onViveUseRightTriggerClick()
			.onWMRUseRightTriggerValue()
			.onOculusGoUseRightTriggerClick()
			.onOculusTouchUseRightTriggerValue()
			.onIndexUseRightTriggerClick()
			.addBooleanAction("left_cancel", "Left Hand Cancel")
			.onDaydreamUseLeftTrackpadClick()
			.onViveUseLeftTrackpadClick()
			.onWMRUseLeftTrackpadClick()
			.onOculusGoUseLeftBackClick()
			.onOculusTouchUseLeftYClick()
			.onIndexUseLeftAClick()
			.addBooleanAction("right_cancel", "Right Hand Cancel")
			.onDaydreamUseRightTrackpadClick()
			.onViveUseRightTrackpadClick()
			.onWMRUseRightTrackpadClick()
			.onOculusGoUseRightBackClick()
			.onOculusTouchUseRightBClick()
			.onIndexUseRightAClick()
			.addBooleanAction("left_shot", "Left Hand Shot")
			.onDaydreamUseLeftSelectClick()
			.onViveUseLeftTriggerClick()
			.onWMRUseLeftTriggerValue()
			.onOculusGoUseLeftTriggerClick()
			.onOculusTouchUseLeftTriggerValue()
			.onIndexUseLeftTriggerClick()
			.addBooleanAction("right_shot", "Right Hand Shot")
			.onDaydreamUseRightSelectClick()
			.onViveUseRightTriggerClick()
			.onWMRUseRightTriggerValue()
			.onOculusGoUseRightTriggerClick()
			.onOculusTouchUseRightTriggerValue()
			.onIndexUseRightTriggerClick()
			.addBooleanAction("left_interact", "Left Hand Interaction")
			.onDaydreamUseLeftTrackpadClick()
			.onViveUseLeftTrackpadClick()
			.onWMRUseLeftTrackpadClick()
			.onOculusGoUseLeftTrackpadClick()
			.onOculusTouchUseLeftSqueezeValue()
			.onIndexUseLeftTrackpadForce()
			.addBooleanAction("right_interact", "Right Hand Interaction")
			.onDaydreamUseRightTrackpadClick()
			.onViveUseRightTrackpadClick()
			.onWMRUseRightTrackpadClick()
			.onOculusGoUseRightTrackpadClick()
			.onOculusTouchUseRightSqueezeValue()
			.onIndexUseRightTrackpadForce()
			.addFloatAction("turn", "Turn")
			.onDaydreamUseRightTrackpadXValue()
			.onViveUseRightTrackpadXValue()
			.onWMRUseRightThumbstickXValue()
			.onOculusTouchUseRightThumbstickXValue()
			.onOculusGoUseRightTrackpadXValue()
			.onIndexUseRightThumbstickXValue()
			.addVector2fAction("move", "Move")
			.onDaydreamUseLeftTrackpadXYValues()
			.onViveUseLeftTrackpadXYValues()
			.onWMRUseLeftThumbstickXYValues()
			.onOculusTouchUseLeftThumbstickXYValues()
			.onOculusGoUseLeftTrackpadXYValues()
			.onIndexUseLeftThumbstickXYValues()
			.addBooleanAction("reset", "Reset VR")
			.onViveUseLeftSqueezeClick()
			.onWMRUseLeftThumbstickClick()
			.onOculusTouchUseLeftThumbstickClick()
			.onIndexUseLeftBClick()
			.addBooleanAction("nextarea", "next Area")
			.onIndexUseRightAClick()
			.addPoseAction("left_aim", "Left Hand Aim", xrSession)
			.useLeftHandAim()
			.addPoseAction("right_aim", "Right Hand Aim", xrSession)
			.useRightHandAim()
			.addPoseAction("left_grip", "Left Hand Grip", xrSession)
			.useLeftHandGrip()
			.addPoseAction("right_grip", "Right Hand Grip", xrSession)
			.useRightHandGrip()
			.suggestBindings();

		xrSession.attach(xrActionSet);
	}

	public void run(@Nonnull DataContainer data) throws IOException, InterruptedException {
		this.stage = new Stage();
		this.engine = new FreescapeEngine(stage, data);
		this.renderer = new FreescapeVRRenderer(stage, views);

		this.renderer.getScreen().changeContent(data.getImage(ImageType.TITLE));
		this.currentState = UiState.TITLE;

		renderLoop();
		terminate();
	}

	private void renderLoop() throws InterruptedException {
		while (!xrSession.isEnded()) {
			if (xrSession.isRunning() && window.isClosing()) {
				xrSession.requestExit();
			}
			GLFWRuntime.pollEvents();
			xrInstance.pollEvents();
			if (!xrSession.isRunning()) {
				// Throttle loop since xrWaitFrame won't be called.
				Thread.sleep(250);
			}

			if (UiState.GAME.equals(currentState)) {
				handleGameActions();
				engine.updateState();
			} else {
				handleMenuActions();
			}

			xrSession.waitOnFrame();
			xrSession.beginFrame();
			if (!xrSession.getFrameState().shouldRender()) {
				xrSession.endFrameWithoutRendering();
				continue;
			}
			xrSession.locateViews(xrSession.getFrameState().predictedDisplayTime());
			if (!xrSession.getViewState().isPositionValid() || !xrSession.getViewState().isOrientationValid()) {
				xrSession.endFrameWithoutRendering();
				continue;
			}

			handlePoses(xrSession.getFrameState().predictedDisplayTime());
			handleReset();

			for (int v = 0; v < xrSession.getViewCount(); v++) {
				final OpenXRView view = views[v];
				view.aquireAndWaitImage();
				view.updateLayerView();

				if (UiState.GAME.equals(currentState)) {
					renderer.renderWorld(engine, view, renderLeftHand, renderRightHand);
				} else {
					renderer.renderScreen(view, renderLeftHand, renderRightHand);
				}
				renderer.renderToWindow(view, v, window.getWidth(), window.getHeight());

				view.releaseImage();
			}

			renderer.flush();
			xrSession.endFrame();
		}
	}

	private void terminate() {
		renderer.finish();
		window._GLFWWindow_();
		xrActionSet._OpenXRActionSet_();
		for (OpenXRView view : views) {
			view._OpenXRView_();
		}
		xrSession._OpenXRSession_();
		xrInstance._OpenXRInstance_();
		GLFWRuntime.terminate();
	}

	private void handleGameActions() {
		xrSession.poll(xrActionSet);

		if (xrActionSet.getBooleanState("left_shot").currentState()) {
			engine.shootLeft();
		}
		if (xrActionSet.getBooleanState("right_shot").currentState()) {
			engine.shootRight();
		}

		final BooleanState leftIntegeract = xrActionSet.getBooleanState("left_interact");
		if (leftIntegeract.changedSinceLastSync() && leftIntegeract.currentState()) {
			engine.interactLeft();
			return;
		}
		final BooleanState rightInteract = xrActionSet.getBooleanState("right_interact");
		if (rightInteract.changedSinceLastSync() && rightInteract.currentState()) {
			engine.interactRight();
			return;
		}

		final BooleanState nextArea = xrActionSet.getBooleanState("nextarea");
		if (nextArea.changedSinceLastSync() && nextArea.currentState()) {
			engine.nextArea();
			return;
		}

		final FloatState turn = xrActionSet.getFloatState("turn");
		final float turnState = turn.currentState();
		if (turnState > 0.3) {
			engine.rotateRight();
		} else if (turnState < -0.3) {
			engine.rotateLeft();
		}

		final Vector2fState move = xrActionSet.getVector2fState("move");
		final Vector2fc moveState = move.currentState();
		if (moveState.y() > 0.3) {
			engine.handleMovement(MovementAction.MOVE_FOWARD);
		} else if (moveState.y() < -0.3) {
			engine.handleMovement(MovementAction.MOVE_BACKWARD);
		} else if (moveState.x() < -0.3) {
			engine.handleMovement(MovementAction.STEP_LEFT);
		} else if (moveState.x() > 0.3) {
			engine.handleMovement(MovementAction.STEP_RIGHT);
		}
	}

	private void handleMenuActions() {
		xrSession.poll(xrActionSet);

		final BooleanState leftAccept = xrActionSet.getBooleanState("left_accept");
		final BooleanState rightAccept = xrActionSet.getBooleanState("right_accept");
		final BooleanState leftCancel = xrActionSet.getBooleanState("left_cancel");
		final BooleanState rightCancel = xrActionSet.getBooleanState("right_cancel");
		if (UiState.TITLE.equals(currentState)) {
			if (leftAccept.currentState() || rightAccept.currentState() || leftCancel.currentState()
				|| rightCancel.currentState()) {
				currentState = UiState.GAME;
			}
		} else {
			if (leftCancel.currentState() || rightCancel.currentState()) {
				currentState = UiState.GAME;
			} else if (leftAccept.changedSinceLastSync() && leftAccept.currentState()) {
			} else if (rightAccept.changedSinceLastSync() && rightAccept.currentState()) {
			}
		}
	}

	private boolean initStage = false;
	private final Vector3f hmdPosition = new Vector3f();

	private void handleReset() {
		final BooleanState reset = xrActionSet.getBooleanState("reset");
		if (initStage && !reset.currentState()) {
			return;
		}
		final Seq<Vector3fc> positions = API.Seq(views)
			.map(OpenXRView::getProjectionView)
			.map(ProjectionView::getPosition);
		if (positions.forAll(v -> v.equals(0f, 0f, 0f))) {
			return;
		}
		initStage = true;
		hmdPosition.set(0f);
		positions.forEach(hmdPosition::add);
		hmdPosition.div(2f);
		stage.setPlayerStage(hmdPosition, views[0].getProjectionView().getOrientation());
	}

	private boolean renderLeftHand, renderRightHand;

	private void handlePoses(long displayTime) {
		xrSession.pollPoses(xrActionSet, displayTime);

		final PoseState leftAimState = xrActionSet.getPoseState("left_aim");
		if (leftAimState.isPositionValid() && leftAimState.isOrientationValid()) {
			stage.setLeftAim(leftAimState.getPosition(), leftAimState.getOrientation());
		}

		final PoseState rightAimState = xrActionSet.getPoseState("right_aim");
		if (rightAimState.isPositionValid() && rightAimState.isOrientationValid()) {
			stage.setRightAim(rightAimState.getPosition(), rightAimState.getOrientation());
		}

		final PoseState leftGripState = xrActionSet.getPoseState("left_grip");
		if (leftGripState.isPositionValid() && leftGripState.isOrientationValid()) {
			stage.setLeftGrip(leftGripState.getPosition(), leftGripState.getOrientation());
		}

		final PoseState rightGripState = xrActionSet.getPoseState("right_grip");
		if (rightGripState.isPositionValid() && rightGripState.isOrientationValid()) {
			stage.setRightGrip(rightGripState.getPosition(), rightGripState.getOrientation());
		}

		renderLeftHand = leftAimState.isActive() && leftGripState.isActive();
		renderRightHand = rightAimState.isActive() && rightGripState.isActive();
	}

	private final class OpenXREventListenerImpl implements OpenXREventListener {
		@Override
		public void sessionStateChange(long session, OpenXRSessionState state, long time) {
			if (OpenXRSessionState.READY.equals(state)) {
				xrSession.begin();
			} else if (OpenXRSessionState.STOPPING.equals(state)) {
				xrSession.end();
			}
		}

		@Override
		public void refSpaceChange(long session, int referenceSpaceType, long changeTime, boolean poseValid,
			Vector3fc positionInPreviousSpace, Quaternionfc orientationInPreviousSpace) {
			System.out.printf("Ignoring event type: Reference Space Change%n");
		}

		@Override
		public void interactionProfileChange(long session) {
			System.out.printf("Ignoring event type: Interaction Profile Change%n");
		}

		@Override
		public void instanceLoss(long lossTime) {
			System.err.printf("XrEventDataInstanceLossPending by %d%n", lossTime);
		}

		@Override
		public void eventsLost(int lostEventCount) {
			System.out.printf("%d events lost%n", lostEventCount);
		}
	}

	private static String toSource(int source) {
		switch (source) {
			case GL43C.GL_DEBUG_SOURCE_API:
				return "API";
			case GL43C.GL_DEBUG_SOURCE_WINDOW_SYSTEM:
				return "WINDOW SYSTEM";
			case GL43C.GL_DEBUG_SOURCE_SHADER_COMPILER:
				return "SHADER COMPILER";
			case GL43C.GL_DEBUG_SOURCE_THIRD_PARTY:
				return "THIRD PARTY";
			case GL43C.GL_DEBUG_SOURCE_APPLICATION:
				return "APPLICATION";
			case GL43C.GL_DEBUG_SOURCE_OTHER:
				return "OTHER";
			default:
				return apiUnknownToken(source);
		}
	}

	private static String toType(int type) {
		switch (type) {
			case GL43C.GL_DEBUG_TYPE_ERROR:
				return "ERROR";
			case GL43C.GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
				return "DEPRECATED BEHAVIOR";
			case GL43C.GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
				return "UNDEFINED BEHAVIOR";
			case GL43C.GL_DEBUG_TYPE_PORTABILITY:
				return "PORTABILITY";
			case GL43C.GL_DEBUG_TYPE_PERFORMANCE:
				return "PERFORMANCE";
			case GL43C.GL_DEBUG_TYPE_OTHER:
				return "OTHER";
			case GL43C.GL_DEBUG_TYPE_MARKER:
				return "MARKER";
			default:
				return apiUnknownToken(type);
		}
	}

	private static String toSeverity(int severity) {
		switch (severity) {
			case GL43C.GL_DEBUG_SEVERITY_HIGH:
				return "HIGH";
			case GL43C.GL_DEBUG_SEVERITY_MEDIUM:
				return "MEDIUM";
			case GL43C.GL_DEBUG_SEVERITY_LOW:
				return "LOW";
			case GL43C.GL_DEBUG_SEVERITY_NOTIFICATION:
				return "NOTIFICATION";
			default:
				return apiUnknownToken(severity);
		}
	}
}
